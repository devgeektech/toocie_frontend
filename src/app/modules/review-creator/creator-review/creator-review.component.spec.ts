 import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatorReviewComponent } from './creator-review.component';

describe('CreatorReviewComponent', () => {
  let component: CreatorReviewComponent;
  let fixture: ComponentFixture<CreatorReviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreatorReviewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatorReviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

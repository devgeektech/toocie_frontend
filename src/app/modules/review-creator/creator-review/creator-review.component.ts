import { Component, OnInit, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-creator-review',
  templateUrl: './creator-review.component.html',
  styleUrls: ['./creator-review.component.scss']
})
export class CreatorReviewComponent implements OnInit {
  currentRating = "";
  userType;
  proposalId
  proposalData: any
  remarks = ""
  readOnly = false
  form: FormGroup;
  submitted = false;
  constructor(private apiService: ApiService, private route: ActivatedRoute, private router: Router,
    private cd: ChangeDetectorRef, private tostr: ToastrService, private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    if (this.route.snapshot.params.id != undefined) {
      this.proposalId = this.route.snapshot.params.id
    }

    this.form = this.formBuilder.group({
      remarks: ['', Validators.required],
      rating: [''],
      brandId: [''],

      createrId: [''],

      proposalId: [''],

      campaignId: [''],



    });

    const retrievedString = localStorage.getItem("ugcUserobject");
    const parsedObject = JSON.parse(retrievedString);
    this.userType = parsedObject.userType;

    if(this.userType=='Admin'){
      this.readOnly = true; 
    }
    this.fetchById();

  }

  fetchById() {
    this.apiService.getData(`proposals/${this.proposalId}`).subscribe((result) => {
      this.proposalData = result[0]
      if (this.proposalData.reviews) {
        this.form.patchValue({
          remarks:this.proposalData.reviews.remarks,
          rating:this.proposalData.reviews.rating
        })        
        this.readOnly = true
      }
      this.cd.detectChanges();
    }, error => {
      console.log(error)
    });
  }

  reset() {
  this.form.reset();
  }


  createReview() {

    this.form.get('brandId').setValue(this.proposalData.brandId)
    this.form.get('createrId').setValue(this.proposalData.createrId)
    this.form.get('campaignId').setValue(this.proposalData.campaignId)
    this.form.get('proposalId').setValue(this.proposalId)

    this.submitted = true;

        // stop here if form is invalid
        if (this.form.invalid) {
            return;
        }
    this.apiService.postData('review', this.form.value).subscribe((res: any) => {
      this.tostr.success('Rating has been submitted', '')
      this.router.navigate(['/brand/proposals/list'])

      console.log(res)
      this.remarks = ''
    },

      error => {
        console.log(error)
      })
  }

}

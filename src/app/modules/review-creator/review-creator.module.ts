import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReviewCreatorRoutingModule } from './review-creator-routing.module';
import { CreatorReviewComponent } from './creator-review/creator-review.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {  NgbRatingModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [CreatorReviewComponent],
  imports: [
    CommonModule,
    ReviewCreatorRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbRatingModule,
    NgbModule,
    RouterModule
  ]
})
export class ReviewCreatorModule { }

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreatorReviewComponent } from './creator-review/creator-review.component';

const routes: Routes = [
  {
    path: 'give-rating/:id',
    component: CreatorReviewComponent,

  },
  {
    path: 'view-rating/:id',
    component: CreatorReviewComponent,

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReviewCreatorRoutingModule { }

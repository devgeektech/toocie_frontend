import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-list-transaction',
  templateUrl: './list-transaction.component.html',
  styleUrls: ['./list-transaction.component.scss']
})
export class ListTransactionComponent implements OnInit {
  transactions = [];
  loading=false;
  userType:string;
  constructor(
    private apiService: ApiService,
    private cd  : ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    const userDetail = JSON.parse(localStorage.getItem('ugcUserobject'));
    this.userType = userDetail.userType;
    this.getTransactionList();
  }

  getTransactionList(){
    this.apiService.getData("transaction").subscribe(
      (result: any) => {       
        this.transactions = result;
        this.loading=true;
        this.cd.detectChanges();
       });
  }
}

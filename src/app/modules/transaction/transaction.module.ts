import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TransactionRoutingModule } from './transaction-routing.module';
import { ListTransactionComponent } from './list-transaction/list-transaction.component';
import { NoRecordPlaceholderModule } from '../no-record-placeholder/no-record-placeholder.module';


@NgModule({
  declarations: [ListTransactionComponent],
  imports: [
    CommonModule,
    TransactionRoutingModule,NoRecordPlaceholderModule
  ]
})
export class TransactionModule { }

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss']
})
export class SettingComponent implements OnInit {
  id: number;
  formGroup: FormGroup;
  errorMessage = '';
  hasError : boolean;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private apiService: ApiService,
    private toastr: ToastrService,
  ) { }

  ngOnInit(): void {
    this.getSetting();
    this.loadForm();
  }

  get f() {
    return this.formGroup.controls;
  }

  getSetting() {
    const data = {};
    this.apiService.postData(`setting`,data).subscribe((result : any) => {
      this.formGroup.patchValue({
        tax: result.tax,
        email: result.email,
        commission: result.commission,
        campaignCost:result.campaignCost
      });
    });
  }

  loadForm() {
    this.formGroup = this.fb.group({
      tax: ['', Validators.required],
      commission: ['',Validators.required],
      email:  ['',Validators.required],
      campaignCost: ['',Validators.required]
    });
  }

  reset() {
    this.loadForm();
  }

  save() {
    if (!this.formGroup.valid) {
      return;
    }
    this.create();
  }

  create() {
    const data = {
      tax:this.f.tax.value,
      commission:this.f.commission.value,
      email:this.f.email.value,
      campaignCost:this.f.campaignCost.value
    }
    this.apiService.postData("setting", data).subscribe(
      (result: any) => {
        // Handle result
        this.apiService.success("Setting has been saved successfully.")
        this.router.navigate(['/admin/setting']);

       },
      (error) => {
        // Handle error
        this.hasError = true;
         this.apiService.error(error.error)

      },
     
    );
    

}
}

import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AddTagComponent } from "./tag/add-tag/add-tag.component";
import { ListTagComponent } from "./tag/list-tag/list-tag.component";
import { SettingComponent } from "./setting/setting.component"
import { ListCreaterComponent } from './creater/list-creater/list-creater.component';
import { ViewCreaterComponent } from './creater/view-creater/view-creater.component';
import { ListCampaignComponent } from './campaign/list-campaign/list-campaign.component';
import { ViewCampaignComponent } from './campaign/view-campaign/view-campaign.component';
import { ListBrandComponent } from './brand/list-brand/list-brand.component';
import { ViewBrandComponent } from './brand/view-brand/view-brand.component';
import { ListVideoComponent } from '../brand/video/list-video/list-video.component';
import { AdminreviewComponent } from './adminreview/adminreview.component';
import { ListComponent } from './payment/list/list.component';
import { DetailComponent } from './payment/detail/detail.component';
import { ListProposalComponent } from './proposal/list-proposal/list-proposal.component';
import { ViewProposalComponent } from './proposal/view-proposal/view-proposal.component';

const routes: Routes = [
  {
    path: "tags",
    children: [
      {
        path: "list", component: ListTagComponent, data: {
          breadcrumb: 'Tags'
        }
      },
      {
        path: "add", component: AddTagComponent, data: {
          breadcrumb: 'Add-Tags'
        }
      },
      {
        path: "edit/:id", component: AddTagComponent, data: {
          breadcrumb: 'Update-Tags'
        }
      },
    ],
  },

  {
    path: "adminReview",
    children: [
      {
        path: "list", component: AdminreviewComponent, data: {
          breadcrumb: 'Review'
        }
      }, 
    ],
  },
  {
    path: "setting",
    children: [
      {
        path: "", component: SettingComponent, data: {
          breadcrumb: 'Settings'
        }
      },
    ],
  },
  {
    path: "creator",
    children: [
      {
        path: "list", component: ListCreaterComponent, data: {
          breadcrumb: 'Creators'
        }
      },
      {
        path: "detail/:id", component: ViewCreaterComponent, data: {
          breadcrumb: 'Creator-details'
        }
      }
    ],
  },
  {
    path: "brand",
    children: [
      {
        path: "list", component: ListBrandComponent, data: {
          breadcrumb: 'Brands'
        }
      },
      { path: "detail/:id", component: ViewBrandComponent, data: {
          breadcrumb: 'Brand-detail'
        }}
    ],
  },
  {
    path: "campaigns",
    children: [
      { path: "list", component: ListCampaignComponent, data: {
        breadcrumb: 'Campaigns'
      } },
      { path: "detail/:id", component: ViewCampaignComponent ,data: {
        breadcrumb: 'Campaign-details'
      }}
    ],
  },
  {
    path: "videos",
    children: [
      {
        path: "list", component: ListVideoComponent,
        data: {
          breadcrumb: 'Videos'
        }
      },
    ],
  },
  {
    path: "payments",
    children: [
      {
        path: "list", component: ListComponent,
        data: {
          breadcrumb: 'Payment'
        }
      },
      {
        path: "detail/:id", component: DetailComponent, data: {
          breadcrumb: 'Payment-Detail'
        }
      }
    ],
  },
  {
    path: "proposals",
    children: [
      {
        path: "list", component: ListProposalComponent, data: {
          breadcrumb: 'Proposals'
        }
      },
      {
        path: "detail/:id", component: ViewProposalComponent, data: {
          breadcrumb: 'Proposal-detail'
        }
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule { }

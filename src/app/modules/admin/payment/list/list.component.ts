import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  payments = [];
  loading = false;
  constructor(
    private apiService: ApiService,
    private cd  : ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.getPaymentList();
  }

  getPaymentList(){
    this.apiService.getData("payments").subscribe(
      (result: any) => {       
        this.payments = result;
        this.loading=true;
        this.cd.detectChanges();
       });
  }

  MarkAsComplete(id){
    this.apiService.getData(`payments/updateStatus/${id}`).subscribe(
      (result: any) => {       
        this.getPaymentList();
        this.loading=true;
        this.cd.detectChanges();
       });
  }

}

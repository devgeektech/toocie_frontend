import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  id:string;
  loading = false;
  paymentData;
  constructor(
    private apiService: ApiService,
    private toastr: ToastrService,
    private router: Router,
    private route: ActivatedRoute,
    private cd: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    if (this.route.snapshot.params.id != undefined) {
      this.id = this.route.snapshot.params["id"];
      }
    this.fetchPaymentDetails();
  }

  fetchPaymentDetails() {
    this.apiService.getData(`payments/${this.id}`).subscribe((result) => {
      this.paymentData = result;     
      this.loading=true;
      this.cd.detectChanges();    
    },error =>{
      console.log(error)
    });
  }
}

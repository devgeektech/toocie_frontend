import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule, NgbRatingModule } from '@ng-bootstrap/ng-bootstrap';
import { AdminRoutingModule } from './admin-routing.module';
import { AddTagComponent } from './tag/add-tag/add-tag.component';
import { ListTagComponent } from './tag/list-tag/list-tag.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SettingComponent } from './setting/setting.component';
import { ListCreaterComponent } from './creater/list-creater/list-creater.component';
import { ViewCreaterComponent } from './creater/view-creater/view-creater.component';
import { ListCampaignComponent } from './campaign/list-campaign/list-campaign.component';
import { ViewCampaignComponent } from './campaign/view-campaign/view-campaign.component';
import { ListBrandComponent } from './brand/list-brand/list-brand.component';
import { ViewBrandComponent } from './brand/view-brand/view-brand.component';
import { NoRecordPlaceholderModule } from '../no-record-placeholder/no-record-placeholder.module';
import { AdminreviewComponent } from './adminreview/adminreview.component';
import { ListComponent } from './payment/list/list.component';
import { DetailComponent } from './payment/detail/detail.component';
import { ListProposalComponent } from './proposal/list-proposal/list-proposal.component';
import { ViewProposalComponent } from './proposal/view-proposal/view-proposal.component';
import { DatePipe } from '@angular/common';

@NgModule({
  declarations: [AddTagComponent, ListTagComponent, SettingComponent, ListCreaterComponent, ViewCreaterComponent, ListCampaignComponent, ViewCampaignComponent, ListBrandComponent, ViewBrandComponent, AdminreviewComponent, ListComponent, DetailComponent, ListProposalComponent, ViewProposalComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NoRecordPlaceholderModule,
    NgbRatingModule,
  ],
  providers: [DatePipe],
})
export class AdminModule { }

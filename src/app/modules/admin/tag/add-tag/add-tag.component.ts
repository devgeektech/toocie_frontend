import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, of, Subscription } from 'rxjs';
import { catchError, switchMap, tap } from 'rxjs/operators';
import { ApiService } from 'src/app/services/api.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-tag',
  templateUrl: './add-tag.component.html',
  styleUrls: ['./add-tag.component.scss']
})

export class AddTagComponent implements OnInit {
  id: number;
  formGroup: FormGroup;
  isLoading$: Observable<boolean>;
  errorMessage = '';
  hasError : boolean;
  constructor(
    private fb: FormBuilder,
    //private productsService: ProductsService,
    private router: Router,
    private route: ActivatedRoute,
    private apiService: ApiService,
    private toastr: ToastrService,
  ) { }

  ngOnInit(): void {
    //this.isLoading$ = this.productsService.isLoading$;
    if (this.route.snapshot.params.id != undefined) {
      this.id = this.route.snapshot.params["id"];
  
      this.fetchById();
      }
    this.loadForm();
  }

  get f() {
    return this.formGroup.controls;
  }

  fetchById() {
    
    this.apiService.getData(`tags/${this.id}`).subscribe((result) => {
      this.formGroup.patchValue({
        name: result.name,
      });
    });
  }

  loadForm() {

    this.formGroup = this.fb.group({
      name: ['', Validators.required]
    });
  }

  reset() {
    this.loadForm();
  }

  save() {
    
    if (!this.formGroup.valid) {
      return;
    }
    console.log("this.id",this.id);
    const formValues = this.formGroup.value;
    if (this.id) {
      this.edit();
    } else {
      this.create();
    }
  }

  edit() {
    console.log("fasf");
    const data = {
      name:this.f.name.value
    }
    this.apiService.postData(`tags/${this.id}`, data).subscribe(
      (result: any) => {console.log('result', result);
        // Handle result
        this.apiService.success('Tag has been updated successfully.');

        this.router.navigate(['/admin/tags/list']);

       },
      (error) => {
        // Handle error
        this.hasError = true;
        this.apiService.error(error.error);
        
      },
     
    );
  }

  create() {
    const data = {
      name:this.f.name.value
    }
    this.apiService.postData("tags", data).subscribe(
      (result: any) => {console.log('result', result);
        // Handle result
        this.apiService.success('Tag has been created successfully.');

        this.router.navigate(['/admin/tags/list']);
       },
      (error) => {
        // Handle error
        this.hasError = true;
        this.apiService.error(error.error);
        
      }
    );
 
  }
}

import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-tag',
  templateUrl: './list-tag.component.html',
  styleUrls: ['./list-tag.component.scss']
})
export class ListTagComponent implements OnInit {
  tags = [];
  loading = false;
  constructor(
    private apiService: ApiService,
    private toastr: ToastrService,
    private router: Router,
    private cd: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.getTagsList()
  }

  getTagsList() {
    this.apiService.getData("tags").subscribe(
      (result: any) => {
        this.tags = result;
        this.loading = true;
        this.cd.detectChanges();
        // Handle result
      });
  }

  deleteData(val,index) {
    this.apiService.swalConfiramation().then((res: any) => {

      if (res.value) {
        this.apiService.deleteData(`tags/${val}`).subscribe(
          (result: any) => {
            // Handle result
            this.apiService.success('Tag has been deleted')
            this.tags.splice(index, 1)
          },
          (error) => {
            // Handle error
            this.apiService.error(error.error)
          },

        );
      }
    })
  }
}

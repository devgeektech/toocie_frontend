import { Component, ChangeDetectorRef,OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-adminreview',
  templateUrl: './adminreview.component.html',
  styleUrls: ['./adminreview.component.scss']
})
export class AdminreviewComponent implements OnInit {
  reviews = [];
  loading = false;
  constructor(   private apiService: ApiService,
    private toastr: ToastrService,
    private cd: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.getadminReviewList();
  }
  getadminReviewList(){
    this.apiService.getData("review/adminreviewList").subscribe(
      (result: any) => {
        console.log("resultadmin",result);
       
        this.reviews = result;
        this.loading=true;
        this.cd.detectChanges();
       });
  }

}

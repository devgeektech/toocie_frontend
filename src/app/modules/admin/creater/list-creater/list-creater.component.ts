import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-creater',
  templateUrl: './list-creater.component.html',
  styleUrls: ['./list-creater.component.scss']
})
export class ListCreaterComponent implements OnInit {
  creaters: any;
  loading = false;
  constructor(
    private apiService: ApiService,
    private toastr: ToastrService,
    private router: Router,
    private cd: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.getCreatersList()
  }

  getCreatersList() {
    this.apiService.getData("creator").subscribe(
      (result: any) => {
        this.creaters = result;
        this.loading = true;
        this.cd.detectChanges();
        // Handle result
      });
  }

  deleteData(val, index) {
    this.apiService.swalConfiramation().then((res: any) => {
      if (res.value) {
        this.apiService.deleteData(`creator/${val}`).subscribe(
          (result: any) => {
             // Handle result
            this.loading = true;
            this.apiService.success('Creator has been deleted successfully.')
            this.creaters.splice(index, 1)

            this.cd.detectChanges();
            // Handle result
          },

          error => {
            this.apiService.error(error.error);
           });
      }
    })

  }


}

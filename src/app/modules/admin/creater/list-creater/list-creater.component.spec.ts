import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListCreaterComponent } from './list-creater.component';

describe('ListCreaterComponent', () => {
  let component: ListCreaterComponent;
  let fixture: ComponentFixture<ListCreaterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListCreaterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListCreaterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, ElementRef, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router'
import { FalseLiteral } from 'typescript';
import { Country,State } from 'country-state-city';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-view-creater',
  templateUrl: './view-creater.component.html',
  styleUrls: ['./view-creater.component.scss']
})
export class ViewCreaterComponent implements OnInit {
  id: string;
  createrData: any;
  loading = false;
  videoSource: string;
  dob="";
  countryName="";
  stateName="";
  @ViewChild('videoPlayer') videoplayer: ElementRef;
  constructor(
    private apiService: ApiService,
    private toastr: ToastrService,
    private router: Router,
    private route: ActivatedRoute,
    private cd: ChangeDetectorRef,
    private datePipe: DatePipe
  ) { }

  ngOnInit(): void {
    if (this.route.snapshot.params.id != undefined) {
      this.id = this.route.snapshot.params["id"];
    }
    this.fetchById();
  }

  fetchById() {
    this.apiService.getData(`creator/${this.id}`).subscribe((result) => {
      this.createrData = result[0];
      this.loading = true;
      this.videoSource = result[0].profileVideo;
      if(result[0].users[0].dob.year){
        console.log("result[0]",result[0].users[0].dob);
        let birthdate = new Date(result[0].users[0].dob.year,(result[0].users[0].dob.month-1),result[0].users[0].dob.day);
        this.dob = this.datePipe.transform(birthdate,'MMM d, y');
      }
      
      if(result[0].users[0].country){
        let country = Country.getCountryByCode(result[0].users[0].country);
        this.countryName = country.name;        
        let state = State.getStateByCodeAndCountry(result[0].users[0].state,country.isoCode);
        this.stateName = state.name;
      }
      
      this.cd.detectChanges();


    }, error => {
      console.log(error)
    });
  }

  approve() {
    this.apiService.postData(`creator/${this.id}/updateStatus`, { status: 1 }).subscribe(
      (result: any) => {
        this.apiService.success('Status has been updated successfully.')
        this.router.navigate(['/admin/creator/list']);
      },
      (error) => {
        // Handle error
        this.apiService.error(error.error)

      },

    );
  }

  reject() {
    this.apiService.postData(`creator/${this.id}/updateStatus`, { status: 2 }).subscribe(
      (result: any) => {

        this.apiService.success('Status has been updated successfully.')
        this.router.navigate(['/admin/creator/list']);

      },
      (error) => {
        // Handle error
        this.apiService.error(error.error)

      },

    );
  }

  toggleVideo() {
    this.videoplayer.nativeElement.play();
  }

}

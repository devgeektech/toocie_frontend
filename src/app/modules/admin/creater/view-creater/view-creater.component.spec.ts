import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewCreaterComponent } from './view-creater.component';

describe('ViewCreaterComponent', () => {
  let component: ViewCreaterComponent;
  let fixture: ComponentFixture<ViewCreaterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewCreaterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewCreaterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

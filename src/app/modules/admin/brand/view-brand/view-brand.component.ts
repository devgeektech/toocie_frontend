import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router'

@Component({
  selector: 'app-view-brand',
  templateUrl: './view-brand.component.html',
  styleUrls: ['./view-brand.component.scss']
})
export class ViewBrandComponent implements OnInit {
  id:string;
  brandData:any;
  loading = false;
  constructor(
    private apiService: ApiService,
    private toastr: ToastrService,
    private router: Router,
    private route: ActivatedRoute,
    private cd: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    if (this.route.snapshot.params.id != undefined) {
      this.id = this.route.snapshot.params["id"];
      }
    this.getBrandDetail();
  }

  getBrandDetail() {
    this.apiService.getData(`brand/${this.id}`).subscribe((result) => {
      this.brandData = result[0];
      this.loading=true;
      this.cd.detectChanges();

      // this.formGroup.patchValue({
      //   name: result.name,
      // });
    },error =>{
      console.log(error)
    });
  }

}

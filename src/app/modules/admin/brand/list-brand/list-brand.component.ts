import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-list-brand',
  templateUrl: './list-brand.component.html',
  styleUrls: ['./list-brand.component.scss']
})
export class ListBrandComponent implements OnInit {
  brands: any;
  loading = false;
  constructor(
    private apiService: ApiService,
    private toastr: ToastrService,
    private router: Router,
    private cd: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.getBrandList()
  }

  getBrandList() {
    this.apiService.getData("brand").subscribe(
      (result: any) => {
        this.brands = result;
        this.loading = true;
        this.cd.detectChanges();
        // Handle result
      });
  }

  deleteData(val, index) {
  this.apiService.swalConfiramation().then((e: any) => {

      if (e.value) {
        this.apiService.deleteData(`brand/${val}`).subscribe(
          (result: any) => {
            // Handle result
            this.loading = true;
            this.brands.splice(index, 1);
            this.apiService.success('Brand has been deleted successfully.');
            this.cd.detectChanges();
          },
          error => {
            this.apiService.error(error.error)
          });
      }

    })

  }

}

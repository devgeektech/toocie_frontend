import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router'

@Component({
  selector: 'app-view-campaign',
  templateUrl: './view-campaign.component.html',
  styleUrls: ['./view-campaign.component.scss']
})
export class ViewCampaignComponent implements OnInit {
  id: string;
  productId: string;
  campaignData: any;
  productName: string;
  loading = false;
  adGoals: any = [];
  adMentions: any = [];
  constructor(
    private apiService: ApiService,
    private toastr: ToastrService,
    private router: Router,
    private route: ActivatedRoute,
    private cd: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    if (this.route.snapshot.params.id != undefined) {
      this.id = this.route.snapshot.params["id"];
    }
    this.fetchById();
  }

  fetchById() {
    this.apiService.getData(`campaigns/${this.id}`).subscribe((result) => {
      this.campaignData = result;
      this.productId = result.productId;
      this.adGoals = result.adGoals;
      this.adMentions = result.adMentions;
      this.loading = true;
      this.cd.detectChanges();
      this.getProductDetails();
      // this.formGroup.patchValue({
      //   name: result.name,
      // });
    }, error => {
      console.log(error)
    });
  }

  getProductDetails() {
    this.apiService.getData(`products/${this.productId}`).subscribe((result) => {
      this.productName = result.productName;
      this.cd.detectChanges();
    });
  }

  approve() {
    this.apiService.postData(`campaigns/${this.id}/updateStatus`, { status: 1 }).subscribe(
      (result: any) => {
        console.log('result', result);
        // Handle result
        this.apiService.success('Campaign has been updated successfully.')
        this.cd.detectChanges();

        this.router.navigate(['/admin/campaigns/list']);

      },
      (error) => {
        // Handle error
        this.apiService.error(error.error)

      },

    );
  }

  reject() {
    this.apiService.postData(`campaigns/${this.id}/updateStatus`, { status: 2 }).subscribe(
      (result: any) => {
        this.apiService.success('Campaign has been updated successfully.')
        this.cd.detectChanges();

        this.router.navigate(['/admin/campaigns/list']);
      },
      (error) => {
        // Handle error
        this.apiService.error(error.error)
      },
     
    );
  }


}

import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-campaign',
  templateUrl: './list-campaign.component.html',
  styleUrls: ['./list-campaign.component.scss']
})
export class ListCampaignComponent implements OnInit {
  campaigns = [];
  loading = false;
  constructor(
    private apiService: ApiService,
    private toastr: ToastrService,
    private router: Router,
    private cd: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.getCampaignList()
  }

  getCampaignList(){
    this.apiService.getData("campaigns").subscribe(
      (result: any) => {
        this.campaigns = result;
        this.loading = true;
        this.cd.detectChanges();
        // Handle result
       });
  }

  deleteData(val,index) {

    this.apiService.swalConfiramation().then((res: any) => {

      if (res.value) {
        this.apiService.deleteData(`campaigns/${val}`).subscribe(
          (result: any) => {

            this.apiService.success('Campaign has been deleted successfully.')
            this.campaigns.splice(index,1)
          },
          (error) => {
            this.apiService.error(error.error)
          },

        );

      }

    })
  }
  
}

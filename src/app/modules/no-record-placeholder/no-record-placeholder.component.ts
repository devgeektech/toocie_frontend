import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { NoRecordFoundText } from '../../constant';

@Component({
  selector: 'app-no-record-placeholder',
  templateUrl: './no-record-placeholder.component.html',
  styleUrls: ['./no-record-placeholder.component.scss']
})
export class NoRecordPlaceholderComponent implements OnInit {
  @Input() title: string
  breadcrumb:any;
  constructor() { }

  ngOnInit(): void {
    this.breadcrumb = NoRecordFoundText;
  }

}

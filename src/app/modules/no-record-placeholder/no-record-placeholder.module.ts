import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NoRecordPlaceholderComponent } from './no-record-placeholder.component';



@NgModule({
  declarations: [NoRecordPlaceholderComponent],
  imports: [
    CommonModule
  ],
  exports: [
    NoRecordPlaceholderComponent
  ]
})
export class NoRecordPlaceholderModule { }

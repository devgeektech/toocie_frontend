import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NoRecordPlaceholderComponent } from './no-record-placeholder.component';

describe('NoRecordPlaceholderComponent', () => {
  let component: NoRecordPlaceholderComponent;
  let fixture: ComponentFixture<NoRecordPlaceholderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NoRecordPlaceholderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoRecordPlaceholderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ConfirmPasswordValidator } from './confirm-password.validator';
import { ApiService } from 'src/app/services/api.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-registration-creater',
  templateUrl: './registration-creater.component.html',
  styleUrls: ['./registration-creater.component.scss']
})
export class RegistrationCreaterComponent implements OnInit {
  registrationForm: FormGroup;
  hasError: boolean;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private apiService: ApiService,
    private toastr:ToastrService
  ) { }

  ngOnInit(): void {
    this.initForm();
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.registrationForm.controls;
  }

  initForm() {
    this.registrationForm = this.fb.group(
      {
        firstName: [
          '',
          Validators.compose([
            Validators.required,
          ]),
        ],
        lastName: [
          '',
          Validators.compose([
            Validators.required,
          ]),
        ],
        email: [
          '',
          Validators.compose([
            Validators.required,
            Validators.email,
            Validators.minLength(3),
            Validators.maxLength(320), // https://stackoverflow.com/questions/386294/what-is-the-maximum-length-of-a-valid-email-address
          ]),
        ],
        password: [
          '',
          Validators.compose([
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(100),
          ]),
        ],
        cPassword: [
          '',
          Validators.compose([
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(100),
          ]),
        ],
        agree: [false, Validators.compose([Validators.required])],
      },
      {
        validator: ConfirmPasswordValidator.MatchPassword,
      }
    );
  }
  /**
   * @param {object} data - user input
   * @param {object} result - user details
   */
  submit() {
    this.hasError = false;
    const data = {
      firstName: this.f.firstName.value,
      lastName: this.f.lastName.value,
      email: this.f.email.value,
      password: this.f.password.value,
      userType:'Creater'
    };

    this.apiService.postData("auth/register", data).subscribe(
      (result: any) => {
        // Handle result
       },
      (error) => {
        // Handle error
        this.hasError = true;
        console.log("error.error",error);
        this.toastr.error(error.error, "Error!");
      },
      () => {
        //this.toastr.success("Please verify your email.", "Success!");
        this.toastr.success("Your account has been created successfully.", "Success!");
        this.router.navigate(['/auth/login']);
        // 'onCompleted' callback.
        // No errors, route to new page here
      }
    );
  }


}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrationCreaterComponent } from './registration-creater.component';

describe('RegistrationCreaterComponent', () => {
  let component: RegistrationCreaterComponent;
  let fixture: ComponentFixture<RegistrationCreaterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistrationCreaterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrationCreaterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

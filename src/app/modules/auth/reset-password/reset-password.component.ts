import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { ConfirmPasswordValidator } from './confirm-password.validator';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
  resetPasswordForm: FormGroup;
  hasError: boolean;
  isLoading$: Observable<boolean>;
  token:string;
  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private apiService: ApiService,
    private toastr: ToastrService,
  ) { }

  ngOnInit(): void {
    if (this.route.snapshot.params.code != undefined) {
        this.token = this.route.snapshot.params["code"]; 
      }
    this.loadForm();
  }

    // convenience getter for easy access to form fields
    get f() {
      return this.resetPasswordForm.controls;
    }

  loadForm() {
    this.resetPasswordForm = this.fb.group({
      password: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(50),
        ]),
      ],
      confirmPassword: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(50),
        ]),
      ]
    },
    {
      validator: ConfirmPasswordValidator.MatchPassword,
    }
    );
  }

  submit() {
    this.hasError = false;
    const data = {
      password: this.f.password.value,
      token: this.token   
    };
    this.apiService.postData("auth/resetPassword", data).subscribe(
      (result: any) => {   
        if(result.responseCode===200){
          // Handle result
         console.log("result",result);
        } 
      },
      (error) => {
        // Handle error
        this.hasError = true;    
        console.log("error",error);  
        this.toastr.error(error.error, "Error!");
        console.log("error inside");
      },
      () => {
        this.toastr.success("You password is reset successfully", "Success!");        
        this.router.navigate(['/']);       
        // 'onCompleted' callback.
        // No errors, route to new page here
      }
    );
  }


}

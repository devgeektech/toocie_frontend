import { AbstractControl } from '@angular/forms';

export class ConfirmPasswordValidator {
  /**
   * Check matching password with confirm password
   * @param control AbstractControl
   */
  static MatchPassword(control: AbstractControl) {
    const password = control.get('password').value;

    const confirmPassword = control.get('cPassword').value;

    if (password !== confirmPassword) {
      control.get('cPassword').setErrors({ ConfirmPassword: true });
    } else {
      return null;
    }
  }
  /**
   * Check matching password with confirm password
   * @param control AbstractControl
   */
   static MatchaccountNumber(control: AbstractControl) {
    const accountNumber = control.get('accountNumber').value;

    const confirmaccountNumber = control.get('confirmaccountNumber').value;

    if (accountNumber !== confirmaccountNumber) {
      control.get('confirmaccountNumber').setErrors({ confirmaccountNumber: true });
    } else {
      return null;
    }
  }
}

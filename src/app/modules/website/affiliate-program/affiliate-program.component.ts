import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-affiliate-program',
  templateUrl: './affiliate-program.component.html',
  styleUrls: ['./affiliate-program.component.scss']
})
export class AffiliateProgramComponent implements OnInit {
  loginForm: FormGroup;
  hasError: boolean;
  returnUrl: string;
  userType: string;
  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private apiService: ApiService,
    private toastr: ToastrService,
  ) { }

  ngOnInit(): void {
    this.loadForm();
  }

  // convenience getter for easy access to form fields
   get f() {
    return this.loginForm.controls;
  }

  loadForm() {
    this.loginForm = this.fb.group({
      name: [
        '', 
        Validators.required
      ],
      email: [
        '',
        Validators.compose([
          Validators.required,
          Validators.email,
          Validators.minLength(3),
          Validators.maxLength(150),
        ])
      ],
      message: [
        '',
        Validators.required
      ]
    });
  }

  submit() {
    this.loginForm.markAllAsTouched();
    if (!this.loginForm.valid) {
      return;
    }
    const data = {
      name: this.f.name.value,
      email: this.f.email.value,
      message:  this.f.message.value
    };
   
    this.apiService.postData("auth/contact", data).subscribe(
      (result: any) => {console.log('result', result);
        // Handle result
        console.log("result",result);
       },
      (error) => {
        // Handle error
        this.hasError = true;
        this.toastr.error(error.error, "Error!");
        console.log(error.error);
        console.log("error inside");
      },
      () => {
        this.toastr.success("Data submitted successfully", "Success!");
        this.router.navigate(['/']);
        // 'onCompleted' callback.
        // No errors, route to new page here
      }
    );
  
  }

}

import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "../website/home/home.component";
import { AboutUsComponent } from "./about-us/about-us.component";
import { PricingComponent } from "./pricing/pricing.component";
import { ContactUsComponent } from "./contact-us/contact-us.component";
import { PlatformComponent } from "./platform/platform.component";
import { BlogComponent } from "./blog/blog.component";
import { AffiliateProgramComponent } from "./affiliate-program/affiliate-program.component";
import { TermsComponent } from "./terms/terms.component";
import { PrivacyComponent } from "./privacy/privacy.component";
import { FaqComponent } from "./faq/faq.component";
import { CreatorComponent } from "./creator/creator.component";
import { BlogDetailComponent } from "./blog-detail/blog-detail.component";

const routes: Routes = [
  {
    path: "",
    component: HomeComponent,
  },
  {
    path: "about",
    component: AboutUsComponent,
  },
  {
    path: "pricing",
    component: PricingComponent,
  },
  {
    path: "contact",
    component: ContactUsComponent,
  },
  {
    path: "platform",
    component: PlatformComponent,
  },
  {
    path: "faq",
    component: FaqComponent,
  },
  {
    path: "affiliate-program",
    component: AffiliateProgramComponent,
  },
  {
    path: "terms-condition",
    component: TermsComponent,
  },
  {
    path: "privacy",
    component: PrivacyComponent,
  },
  {
    path: "blog",
    component: BlogComponent,
  },
  {
    path: "blog-detail",
    component: BlogDetailComponent,
  },
  {
    path: "creatur",
    component: CreatorComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WebsiteRoutingModule {}

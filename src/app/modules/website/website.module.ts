import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { WebsiteRoutingModule } from "./website-routing.module";
import { HomeComponent } from "./home/home.component";
import { AboutUsComponent } from "./about-us/about-us.component";
import { PricingComponent } from "./pricing/pricing.component";
import { ContactUsComponent } from "./contact-us/contact-us.component";
import { OuterHeaderComponent } from "src/app/pages/_layout/components/outer-header/outer-header.component";
import { OuterFooterComponent } from "src/app/pages/_layout/components/outer-footer/outer-footer.component";
import { PlatformComponent } from './platform/platform.component';
import { BlogComponent } from './blog/blog.component';
import { AffiliateProgramComponent } from './affiliate-program/affiliate-program.component';
import { TermsComponent } from './terms/terms.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { FaqComponent } from './faq/faq.component';
import { CreatorComponent } from './creator/creator.component';
import { BlogDetailComponent } from './blog-detail/blog-detail.component';



@NgModule({
  declarations: [
    HomeComponent,
    AboutUsComponent,
    PricingComponent,
    ContactUsComponent,
    OuterHeaderComponent,
    OuterFooterComponent,
    PlatformComponent,
    BlogComponent,
    AffiliateProgramComponent,
    TermsComponent,
    PrivacyComponent,
    FaqComponent,
    CreatorComponent,
    BlogDetailComponent
    
    
  ],

  exports:[OuterHeaderComponent,OuterFooterComponent],
  imports: [CommonModule, WebsiteRoutingModule,FormsModule,
    ReactiveFormsModule],
})
export class WebsiteModule {}

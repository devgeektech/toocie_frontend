import { ElementRef } from '@angular/core';
import { ViewChild } from '@angular/core';
import { OnDestroy } from '@angular/core';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit,OnDestroy {
  userName = '';
  @ViewChild('videoPlayer1') videoplayer1: ElementRef;
  @ViewChild('videoPlayer2') videoplayer2: ElementRef;
  @ViewChild('videoPlayer3') videoplayer3: ElementRef;
  video1 ="play";
  video2 ="play";
  video3 ="play";
  constructor() { }
  ngOnInit(): void {
    let userObject: any = localStorage.getItem('ugcUserobject');
    
    if(userObject != undefined){
      //console.log('userObject', userObject);
      userObject = JSON.parse(userObject);
      this.userName = userObject.firstName;
    }
    
  }
  ngOnDestroy(): void {
    document.body.style.backgroundImage = '';
  }

  playVideo(id,btn){
    console.log("this[id]",id);
    if (this[id].nativeElement.paused){
      this[id].nativeElement.play(); 
      this[btn] = 'pause';
    } 
    else{
      this[id].nativeElement.pause(); 
      this[btn] = 'play';
    }
    
  
     //this[id].nativeElement.play();
  }
}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent implements OnInit {
  userName = '';
  constructor() { }

  ngOnInit(): void {
    let userObject: any = localStorage.getItem('ugcUserobject');
    if(userObject != undefined){
      console.log('userObject', userObject);
      userObject = JSON.parse(userObject);
      this.userName = userObject.firstName;
    }
  }
 

}

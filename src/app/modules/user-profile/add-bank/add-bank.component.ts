import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { AuthService, UserModel, ConfirmPasswordValidator } from '../../auth';
import { DOCEXTENTIONTYPE, DOCSIZEERRORMESSAGE, DOCFILESIZE, DOCERRORMESSAGE } from 'src/app/constant';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-add-bank',
  templateUrl: './add-bank.component.html',
  styleUrls: ['./add-bank.component.scss']
})
export class AddBankComponent implements OnInit {
  formGroup: FormGroup;
  hasError: boolean;
  loading=false;
  bankDetail:any;
  detail:any;
  showField = true;
  buttonShowHide:any;
  accountStatus:any;
  userCountry:string;
  isVerified:any;
  constructor(private userService: AuthService, 
    private fb: FormBuilder,
    private router: Router,
    private apiService: ApiService,
    private toastr:ToastrService,
    private cd:ChangeDetectorRef
    ) { }

  ngOnInit(): void {
    this.loadForm();
    this.fetchAccountDetail();
    const userDetail = JSON.parse(localStorage.getItem("ugcUserobject"));
   
    this.userCountry = userDetail.country;

    if (this.userCountry == "US") {
      this.formGroup.get("routingNumber").setValidators(Validators.required);
      this.formGroup.get("routingNumber").updateValueAndValidity();      
    }

  }
   // convenience getter for easy access to form fields
   get f() {
    return this.formGroup.controls;
  }
  loadForm() {
    this.formGroup = this.fb.group({
      accountholderName: [ '',  Validators.required ],
      routingNumber: [ ''],
      accountNumber: ['',  Validators.required],
      confirmaccountNumber: ['', Validators.required],
      front: [''],
      back: ['']
    }, 
    {
      validator: ConfirmPasswordValidator.MatchaccountNumber
    }
    );
  }

   /**
   * Fetch account information of logged in user
   * @param {string} token - jwt token
   * @param {object} result - user details
   */
  fetchAccountDetail() {
    this.apiService.getData(`auth/accountDetail`).subscribe((result : any) => {
      console.log("result",result);
      this.bankDetail = result;
      if(!result.accountNumber){
        this.detail = "d-none";
        this.showField = false;
        this.buttonShowHide = "d-inline";
      }
      
      this.loading = true;
      this.cd.detectChanges();
      this.formGroup.patchValue({
        accountholderName: result.accountHolderName,
        routingNumber    :  result.routingNumber  
      });
      this.buttonShowHide = "d-none";
    },
    (error) => {
      // Handle error
      
      this.detail = "d-none";
      this.showField = false;
      this.accountStatus = error.error;
      this.buttonShowHide = "d-inline";
    },   
    );
  }


submit(){
  this.formGroup.markAllAsTouched();
    if (!this.formGroup.valid) {
      return;
    } 
    const data = {
      accountholderName: this.f.accountholderName.value,
      routingNumber: this.f.routingNumber.value,
      accountNumber: this.f.accountNumber.value,
      confirmaccountNumber: this.f.confirmaccountNumber.value, 
    };
   
    this.apiService.postData("auth/addBankAccount", data).subscribe(
      (result: any) => {console.log('result', result);
        // Handle result
        console.log("result",result);
       },
      (error) => {
        // Handle error
        this.hasError = true;
        this.toastr.error(error.error, "Error!");
        console.log(error.error);
        console.log("error inside");
      },
      () => {
        this.toastr.success("Bank Detaiks Added successfully.", "Success!");
        //this.router.navigate(['/dashboard']);
        // 'onCompleted' callback.
        // No errors, route to new page here
      }
    );
}

onFileChange(event,type){ 
  
  if (event.target.files.length > 0) { 
    const file = event.target.files[0];
    let extCheck = this.typeValidation(file.name.toLowerCase())
      if (!extCheck) {
        return this.swal(DOCERRORMESSAGE)
      }

      let fileSizeCheck = this.fileSizeValidation(file.size)
      if (!fileSizeCheck) {
        return this.swal(DOCSIZEERRORMESSAGE)
      }

    const formData = new FormData();
    formData.append('type', type);
    for  (var i =  0; i <  event.target.files.length; i++)  {       
      formData.append('file', event.target.files[i]);
    }   

    this.apiService.postDataMultipart("auth/uploadDoc",formData).subscribe(
      (result: any) => {
      this.loading = true;
        // Handle result
       },
      (error) => {
        // Handle error
        this.toastr.error(error.error, "Error!");
      },
      () => {
        this.toastr.success("Doc uploaded successfully.", "Success!");
        this.fetchAccountDetail();
          
      }
    );
  }
}
onchangeAdditionaldoc(event,type){ 
  
  if (event.target.files.length > 0) { 
    const file = event.target.files[0];
    let extCheck = this.typeValidation(file.name.toLowerCase())
      if (!extCheck) {
        return this.swal(DOCERRORMESSAGE)
      }

      let fileSizeCheck = this.fileSizeValidation(file.size)
      if (!fileSizeCheck) {
        return this.swal(DOCSIZEERRORMESSAGE)
      }

    const formData = new FormData();
    formData.append('type', type);
    for  (var i =  0; i <  event.target.files.length; i++)  {       
      formData.append('file', event.target.files[i]);
    }   

    this.apiService.postDataMultipart("auth/uploadDoc",formData).subscribe(
      (result: any) => {
      this.loading = true;
        // Handle result
       },
      (error) => {
        // Handle error
        this.toastr.error(error.error, "Error!");
      },
      () => {
        this.toastr.success("Doc uploaded successfully.", "Success!");
        this.fetchAccountDetail();
          
      }
    );
  }
}
fileSizeValidation(size: number) {

  if (size > DOCFILESIZE
  ) {
    return false
  }
  else {
    return true
  }
}

typeValidation(name: string) {
  let ext = name.substring(name.lastIndexOf('.') + 1);
  const extType = DOCEXTENTIONTYPE
  let res = extType.includes(ext)
  return res

}

swal(message) {
  return Swal.fire({
    icon: 'error',
    confirmButtonColor: '#6993FF',
    text: message,
  })
}

editForm(){
  this.detail = "d-none";
  this.showField = false;
  this.buttonShowHide = "d-inline";
}

}

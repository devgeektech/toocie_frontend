import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountInformationComponent } from './account-information/account-information.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { EmailSettingsComponent } from './email-settings/email-settings.component';
import { PersonalInformationComponent } from './personal-information/personal-information.component';
import { ProfileOverviewComponent } from './profile-overview/profile-overview.component';
import { SavedCreditCardsComponent } from './saved-credit-cards/saved-credit-cards.component';
import { StatementsComponent } from './statements/statements.component';
import { TaxInformationComponent } from './tax-information/tax-information.component';
import { UserProfileComponent } from './user-profile.component';
import { WalletComponent } from './wallet/wallet.component';
import { AddBankComponent } from './add-bank/add-bank.component';

const routes: Routes = [
  {
    path: '',
    component: UserProfileComponent,
    children: [
      {
        path: 'profile-overview',
        component: ProfileOverviewComponent, data: {
          breadcrumb: 'Profile-Overview'
        }
      },
      {
        path: 'personal-information',
        component: PersonalInformationComponent
        , data: {
          breadcrumb: 'Personal-Information'
        }
      },
      {
        path: 'wallet',
        component: WalletComponent,
        data: {
          breadcrumb: 'Wallet'
        }
      },
      {
        path: 'account-information',
        component: AccountInformationComponent,
        data: {
          breadcrumb: 'Account-Information'
        }
      },
      {
        path: 'change-password',
        component: ChangePasswordComponent,
        data: {
          breadcrumb: 'Change-Password'
        }
      },
      {
        path: 'add-bank',
        component: AddBankComponent
      },
      {
        path: 'email-settings',
        component: EmailSettingsComponent,
        data: {
          breadcrumb: 'Email-Settings'
        }
      },
      {
        path: 'saved-credic-cards',
        component: SavedCreditCardsComponent,
        data: {
          breadcrumb: 'Credic-Cards'
        }
      },
      {
        path: 'tax-information',
        component: TaxInformationComponent,
        data: {
          breadcrumb: 'Tax-Information'
        }
      },
      {
        path: 'statements',
        component: StatementsComponent,
        data: {
          breadcrumb: 'Statements'
        }
      },
      { path: '', redirectTo: 'profile-overview', pathMatch: 'full' },
      { path: '**', redirectTo: 'profile-overview', pathMatch: 'full' },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserProfileRoutingModule { }

import { Component, OnInit, ChangeDetectorRef, ViewChild, ɵConsole } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ApiService } from 'src/app/services/api.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { STRIPE_PUBLISHABLE_KEY } from 'src/app/constant';


@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.component.html',
  styleUrls: ['./wallet.component.scss']
})
export class WalletComponent implements OnInit {
  formGroup: FormGroup;
  loading = false;
  handler:any = null;
  amount:number;
  constructor(
    private http: HttpClient,
    private fb: FormBuilder,
    private apiService: ApiService,
    private toastr:ToastrService,
    private router: Router,
    private cd: ChangeDetectorRef
  ) { }

  
  ngOnInit(): void {
    this.loadForm();
    this.loadStripe();
    
  }

  get f() {
    return this.formGroup.controls;
  }

  makePayment(token){
    let data = {
      token: token.id,
      amount: this.amount,
    }
    console.log('token is here', token);
    this.apiService.postData("auth/makePayment", data).subscribe(
      (result: any) => {
      this.loading = true;
        const retrievedString = localStorage.getItem("ugcUserobject");
        const parsedObject = JSON.parse(retrievedString);
        parsedObject.balance = parsedObject.balance+this.amount;
        const strigifiedForStorage = JSON.stringify(parsedObject);
        localStorage.setItem("ugcUserobject", strigifiedForStorage);
        this.apiService.SharingData.next(parsedObject.balance);  

        // Handle result
        this.apiService.success("Fund added successfully.");
        this.router.navigate(['/dashboard']);
       },
      (error) => {
        // Handle error
      
        this.apiService.error(error.error)
      },
  
    );
  }

  pay(amount) {    
    this.amount = amount;
    var handler = (<any>window).StripeCheckout.configure({
      key :STRIPE_PUBLISHABLE_KEY,
      locale: 'auto',
      token: (token: any) => this.makePayment(token)
    });
 
    handler.open({
      name: 'Toocie Payment',
      description: 'Add Fund',
      amount: amount * 100,
      currency:"EUR"
    });
 
  }
  
  loadStripe() {
    if(!window.document.getElementById('stripe-script')) {
      var s = window.document.createElement("script");
      s.id = "stripe-script";
      s.type = "text/javascript";
      s.src = "https://checkout.stripe.com/checkout.js";
      s.onload = () => {
        this.handler = (<any>window).StripeCheckout.configure({
          key: STRIPE_PUBLISHABLE_KEY,
          locale: 'auto',
          token: function (token: any) {
            // You can access the token ID with `token.id`.
            // Get the token ID to your server-side code for use.
            alert('Payment Success!!');
          }
        });
      }
      window.document.body.appendChild(s);
    }
  }

  reset() {
    this.loadForm();
  }
  loadForm(){
    this.loading = true;
    this.formGroup = this.fb.group({
      amount: ['', Validators.required]
    });
  }
  

  save(){
    this.formGroup.markAllAsTouched();
    if (!this.formGroup.valid) {
      return;
    }

    this.pay(this.f.amount.value)

  }

}

import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import { ApiService } from 'src/app/services/api.service';
import { AuthService, UserModel, ConfirmPasswordValidator } from '../../auth';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit, OnDestroy {
  formGroup: FormGroup;
  user: UserModel;
  firstUserState: UserModel;
  subscriptions: Subscription[] = [];
  isLoading$: Observable<boolean>;
  hasError: boolean;
  constructor(
    private userService: AuthService, 
    private fb: FormBuilder,
    private router: Router,
    private apiService: ApiService,
    private toastr:ToastrService
    ) {
    this.isLoading$ = this.userService.isLoadingSubject.asObservable();
  }

  ngOnInit(): void {
    // const userDetail = JSON.parse(localStorage.getItem('ugcUserobject'));
    // this.user = Object.assign({}, userDetail);
    this.loadForm();
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.formGroup.controls;
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sb => sb.unsubscribe());
  }

  loadForm() {
    this.formGroup = this.fb.group({
      currentPassword: [
        '', 
        Validators.required
      ],
      password: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(100),
        ]),
      ],
      cPassword: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(100),
        ]),
      ]
    }, 
    {
      validator: ConfirmPasswordValidator.MatchPassword
    }
    );
  }

  submit() {
    this.formGroup.markAllAsTouched();
    if (!this.formGroup.valid) {
      return;
    }
    const data = {
      currentPassword: this.f.currentPassword.value,
      password: this.f.password.value
    };
    console.log("data",data);

    this.apiService.postData("auth/changePassword", data).subscribe(
      (result: any) => {console.log('result', result);
        // Handle result
        console.log("result",result);
       },
      (error) => {
        // Handle error
        this.hasError = true;
        this.toastr.error(error.error, "Error!");
        console.log(error.error);
        console.log("error inside");
      },
      () => {
        this.toastr.success("Password changed successfully.", "Success!");
        this.router.navigate(['/dashboard']);
        // 'onCompleted' callback.
        // No errors, route to new page here
      }
    );
    // this.user.password = this.formGroup.value.password;
    // this.userService.isLoadingSubject.next(true);
    // setTimeout(() => {
    //   this.userService.currentUserSubject.next(Object.assign({}, this.user));
    //   this.userService.isLoadingSubject.next(false);
    // }, 2000);
  }

  cancel() {
    this.user = Object.assign({}, this.firstUserState);
    this.loadForm();
  }

  // helpers for View
  // isControlValid(controlName: string): boolean {
  //   const control = this.formGroup.controls[controlName];
  //   return control.valid && (control.dirty || control.touched);
  // }

  // isControlInvalid(controlName: string): boolean {
  //   const control = this.formGroup.controls[controlName];
  //   return control.invalid && (control.dirty || control.touched);
  // }

  // controlHasError(validation, controlName): boolean {
  //   const control = this.formGroup.controls[controlName];
  //   return control.hasError(validation) && (control.dirty || control.touched);
  // }

  // isControlTouched(controlName): boolean {
  //   const control = this.formGroup.controls[controlName];
  //   return control.dirty || control.touched;
  // }
}

import {
  Component,
  OnDestroy,
  OnInit,
  ChangeDetectorRef,
  ViewChild,
  ElementRef,
  ChangeDetectionStrategy,
} from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Observable, Subscription } from "rxjs";
import { first } from "rxjs/operators";
import { AuthService, UserModel } from "../../auth";
import { ApiService } from "src/app/services/api.service";
import { ToastrService } from "ngx-toastr";
import { ActivatedRoute, Router } from "@angular/router";
import { VideoRecordingService } from "src/app/services/video-record-service";
import { DomSanitizer, SafeUrl } from "@angular/platform-browser";
import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";
import * as moment from "moment";
import { COUNTRIES } from "src/app/country"
import {
  COUNTRYSTATES,
  FILEERRORMESSAGE,
  FILEEXTENTIONTYPE,
  FILESIZE,
  FILESIZEERRORMESSAGE,
} from "src/app/constant";

import { Country,State } from 'country-state-city';

@Component({
  selector: "app-personal-information",
  templateUrl: "./personal-information.component.html",
  styleUrls: ["./personal-information.component.scss"],
})
export class PersonalInformationComponent implements OnInit, OnDestroy {
  countryList = Country.getAllCountries();
  formGroup: FormGroup;
  user: UserModel;
  firstUserState: UserModel;
  subscriptions: Subscription[] = [];
  avatarPic = "none";
  isLoading$: Observable<boolean>;
  userType: string;
  loading = false;
  files = [];
  data: any;
  videoSource: string;
  videoSourceDislay = true;
  @ViewChild("videoPlayer") videoplayer: ElementRef;
  model: NgbDateStruct;
  statesList = [];
 
  minDate;
  maxDate;
  startDate;
  document_front = [];
  document_back = [];
  /** for recording start */
  video: any;
  private interval: any;
  private strtTime: any;
  url: any;
  isPlaying = false;
  displayControls = true;
  isAudioRecording = false;
  isVideoRecording = false;
  audioRecordedTime: any;
  videoRecordedTime: any;
  audioBlobUrl: any;
  videoBlobUrl: any;
  audioBlob: any;
  videoBlob: any;
  audioName: any;
  videoName: any;
  audioStream: any;
  videoStream: MediaStream | undefined;
  audioConf = { audio: true };
  videoConf = { video: { facingMode: "user", width: 320 }, audio: true };
  @ViewChild("videoElement") videoElement: any;
  /*** for recording end */
  constructor(
    private userService: AuthService,
    private fb: FormBuilder,
    private router: Router,
    private apiService: ApiService,
    private toastr: ToastrService,
    private cd: ChangeDetectorRef,
    private ref: ChangeDetectorRef,
    private route: ActivatedRoute,
    private videoRecordingService: VideoRecordingService,
    private sanitizer: DomSanitizer
  ) {}

  ngOnInit(): void {
    this.fetchById();
    // Load form and make field conditonally required
    this.loadForm();
    if (this.userType == "Brand") {
      this.formGroup.get("companyName").setValidators(Validators.required);
      this.formGroup.get("companyName").updateValueAndValidity();
      this.formGroup.get("website").setValidators(Validators.required);
      this.formGroup.get("website").updateValueAndValidity();
    }
    if (this.userType == "Creater") {
      this.formGroup.get("line1").setValidators(Validators.required);
      this.formGroup.get("line1").updateValueAndValidity();
      this.formGroup.get("line2").setValidators(Validators.required);
      this.formGroup.get("line2").updateValueAndValidity();
      this.formGroup.get("country").setValidators(Validators.required);
      this.formGroup.get("country").updateValueAndValidity();
      this.formGroup.get("city").setValidators(Validators.required);
      this.formGroup.get("city").updateValueAndValidity();
      this.formGroup.get("postal_code").setValidators(Validators.required);
      this.formGroup.get("postal_code").updateValueAndValidity();
      this.formGroup.get("state").setValidators(Validators.required);
      this.formGroup.get("state").updateValueAndValidity();
      this.formGroup.get("dob").setValidators(Validators.required);
      this.formGroup.get("dob").updateValueAndValidity();
    }
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.formGroup.controls;
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sb) => sb.unsubscribe());
  }
  /**
   * Fetch personal information of logged in user
   * @param {string} token - jwt token
   * @param {object} result - user details
   */
  fetchById() {
    this.apiService.getData(`auth/user`).subscribe((result: any) => {
      this.loading = true;
      this.userType = result.userType;
      this.videoSource = result.profileVideo;
      this.cd.detectChanges();
      let countryName = result.country;
      this.statesList = State.getStatesOfCountry(countryName);      
      // if (countryName === "US") {
      //   this.statesList = COUNTRYSTATES[0];
      // } else if (countryName === "BE") {
      //   this.statesList = COUNTRYSTATES[1];
      // }
      this.formGroup.patchValue({
        firstName: result.firstName,
        lastName: result.lastName,
        companyName: result.companyName,
        phone: result.phone,
        email: result.email,
        website: result.website,
        line1: result.line1,
        line2: result.line2,
        country:result.country,
        city: result.city,
        postal_code: result.postal_code,
        state: result.state,
        dob: result.dob
      });
    });
  }

  loadForm() {
    this.minDate = { year: new Date().getFullYear() - 100, month: 1, day: 1 };
    this.maxDate = { year: new Date().getFullYear() - 15, month: 12, day: 31 };
    this.formGroup = this.fb.group({
      firstName: ["", Validators.required],
      lastName: ["", Validators.required],
      companyName: [""],
      profileVideo: [""],
      country:[""],
      phone: ["", Validators.required],
      email: ["", Validators.compose([Validators.required, Validators.email])],
      website: [""],
      line1: [""],
      line2: [""],
      city: [""],
      postal_code: [""],
      state: [""],
      dob: [""],
    });
  }

  /**
   * Update user profile
   * @param {object} data - user inputs
   * @param {object} result - user updated details
   */
  save() {
   
    this.formGroup.markAllAsTouched();
    if (!this.formGroup.valid) {
      return;
    }

    const data = {
      firstName: this.f.firstName.value,
      lastName: this.f.lastName.value,
      companyName: this.f.companyName.value,
      phone: this.f.phone.value,
      email: this.f.email.value,
      website: this.f.website.value,
      userType: this.userType,
      line1: this.f.line1.value,
      line2: this.f.line2.value,
      city: this.f.city.value,
      country: this.f.country.value,
      postal_code: this.f.postal_code.value,
      state: this.f.state.value,
      dob: this.f.dob.value
    };

    this.apiService.postData("auth/updateProfile", data).subscribe(
      (result: any) => {
        this.loading = true;

        const retrievedString = localStorage.getItem("ugcUserobject");
        const parsedObject = JSON.parse(retrievedString);
        parsedObject.country =result.country;
        const strigifiedForStorage = JSON.stringify(parsedObject);
        localStorage.setItem("ugcUserobject", strigifiedForStorage);
        //this.apiService.SharingData.next(parsedObject.country); 

        this.cd.detectChanges();
        // Handle result
      },
      (error) => {
        // Handle error
        this.toastr.error(error.error, "Error!");
        console.log(error.error);
        console.log("error inside");
      },
      () => {
        this.toastr.success("Profile updated successfully.", "Success!");
        this.router.navigate(["/user-profile/personal-information"]);
        // 'onCompleted' callback.
        // No errors, route to new page here
      }
    );
  }

  cancel() {
    this.user = Object.assign({}, this.firstUserState);
    this.loadForm();
  }

  getPic() {
    if (!this.user.pic) {
      return "none";
    }

    return `url('${this.user.pic}')`;
  }

  deletePic() {
    this.user.pic = "";
  }

  onFileChange(event) {
    if (event.target.files.length > 0) {
      let extCheck = this.apiService.typeValidation(
        event.target.files[0].name.toLowerCase()
      );
      if (!extCheck) {
        return this.apiService.swal(FILEERRORMESSAGE);
      }
      let fileSizeCheck = this.apiService.fileSizeValidation(
        event.target.files[0].size
      );
      if (!fileSizeCheck) {
        return this.apiService.swal(FILESIZEERRORMESSAGE);
      }
      // for (var i = 0; i < event.target.files.length; i++) {
      //   this.files = [...this.files, event.target.files[i]];
      // }
      //console.log(this.files);

      const formData = new FormData();
      for (var i = 0; i < event.target.files.length; i++) {
        formData.append("profileVideo",event.target.files[i]);
      }

      this.apiService
        .postDataMultipart("creator/uploadProfileVideo", formData)
        .subscribe(
          (result: any) => {
            this.loading = true;
            this.cd.detectChanges();
            this.videoSource = result.profileVideo;
            //this.fetchById();
            this.videoplayer.nativeElement.load();
            
            // Handle result
          },
          (error) => {
            // Handle error
            this.toastr.error(error.error, "Error!");
          },
          () => {
            this.toastr.success(
              "Profile video saved successfully.",
              "Success!"
            );
            // this.fetchById();
            //this.router.navigate(['/user-profile/personal-information']);
          }
        );
    }
  }

  toggleVideo() {
    this.videoplayer.nativeElement.play();
  }

  /** Functions for recording start  */

  stopVideo() {
    console.log("this.videoBlob", this.videoBlob);
    this.strtTime = moment();
    let time = 0;
    this.interval = setInterval(() => {
      const currentTime = moment();
      const diffTime = moment.duration(currentTime.diff(this.strtTime));
      time = diffTime.seconds();
      console.log(time, "time");
      if (time > 15) {
        this.stopVideoRecording();
        this.isVideoRecording = false;
      }
    }, 500);
    console.log(this.videoBlob, this.videoName, "videoName");
  }
  startVideoRecording() {
    clearTimeout(this.interval);
    this.video = this.videoElement.nativeElement;
    this.videoSourceDislay = false;
    if (!this.isVideoRecording) {
      this.video.controls = false;
      this.isVideoRecording = true;
      this.videoRecordingService
        .startRecording(this.videoConf)
        .then((stream: any) => {
          // this.video.src = window.URL.createObjectURL(stream);
          this.stopVideo();
          console.log("Test", this.videoBlob);
          this.video.srcObject = stream;
          this.video.play();
        })
        .catch(function (err: { name: string; message: string }) {
          console.log(err.name + ": " + err.message);
        });
    }
  }
  startTime(startTime: any): moment.DurationInputArg1 {
    throw new Error("Method not implemented.");
  }

  stopVideoRecording() {
    clearTimeout(this.interval);
    if (this.isVideoRecording) {
      this.videoRecordingService.stopRecording();
      this.videoRecordingService.getRecordedBlob().subscribe((data) => {
        this.downloadVideoRecordedData(data.blob);
        this.clearVideoRecordedData();
        this.ref.detectChanges();
      });
    }
  }

  clearVideoRecordedData() {
    clearTimeout(this.interval);
    this.videoBlobUrl = null;
    this.video.srcObject = null;
    this.video.controls = false;
    this.ref.detectChanges();
  }

  downloadVideoRecordedData(blob) {
    this._downloadFile(blob, "video/mp4", "profile");
  }

  _downloadFile(data: any, type: string, filename: string): any {
    const blob = new Blob([data], { type: type });

    const url = window.URL.createObjectURL(blob);
    //this.video.srcObject = stream;
    //const url = data;
    const anchor = document.createElement("a");
    anchor.download = filename;
    anchor.href = url;
    document.body.appendChild(anchor);
    anchor.click();
    document.body.removeChild(anchor);
  }
  /** end  */

  getStates(event) {
  
    this.statesList = [];
    let countryName = event.target.value;
    this.statesList = State.getStatesOfCountry(countryName);
    // if (countryName === "US") {
    //   this.statesList = COUNTRYSTATES[0];
    // } else if (countryName === "BE") {
    //   this.statesList = COUNTRYSTATES[1];
    // }
  }
}

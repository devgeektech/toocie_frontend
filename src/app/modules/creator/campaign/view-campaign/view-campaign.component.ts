import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/services/api.service';
import { Title } from '@angular/platform-browser';
import { FILEERRORMESSAGE, FILEEXTENTIONTYPE, FILESIZE, FILESIZEERRORMESSAGE } from 'src/app/constant';
@Component({
  selector: 'app-view-campaign',
  templateUrl: './view-campaign.component.html',
  styleUrls: ['./view-campaign.component.scss']
})
export class ViewCampaignComponent implements OnInit {
  activePlatform:any;
  activeVideoFormat:any;
  formGroup: FormGroup;
  id: string;
  productId: string;
  campaignData: any;
  productName: string;
  loading = false;
  adGoals: any = [];
  adMentions: any = [];
  files = [];
  alreadyApplied = false;
  constructor(
    private fb: FormBuilder,
    private apiService: ApiService,
    private toastr: ToastrService,
    private router: Router,
    private route: ActivatedRoute,
    private cd: ChangeDetectorRef
  ) { }

  ngOnInit(): void {


    if (this.route.snapshot.params.id != undefined) {
      this.id = this.route.snapshot.params["id"];
    }
    this.fetchById();
    this.isCampaignAlreadyApplied();
    this.loadForm();
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.formGroup.controls;
  }

  loadForm() {
    this.formGroup = this.fb.group({
      // message: ['', Validators.required],
      // proposalVideo: ['', Validators.required]
      message: [''],
      proposalVideo: [''],
      deliveryAddress: ['', Validators.required]
    });
  }

  fetchById() {
    this.apiService.getData(`campaigns/${this.id}`).subscribe((result) => {
      this.campaignData = result;
      this.productId = result.productId;
      this.adGoals = result.adGoals;
      this.adMentions = result.adMentions;
      this.loading = true;
      this.activePlatform = result.displayPlatForm;
      this.activeVideoFormat = result.videoFormat;
      this.cd.detectChanges();
      this.getProductDetails();
    }, error => {
      console.log(error)
    });
  }

  getProductDetails() {
    this.apiService.getData(`products/${this.productId}`).subscribe((result) => {
      this.productName = result.productName;
      this.cd.detectChanges();
    });
  }
  isCampaignAlreadyApplied() {
    this.apiService.getData(`proposals/${this.id}/alreadyApplied`).subscribe((result) => {
      console.log("result",result);     
      if(result){
        this.formGroup.patchValue({
          deliveryAddress: result.deliveryAddress
        });
        this.alreadyApplied = true;
      }
      this.cd.detectChanges();
    });
  }
  

  onFileChange(event) {
    if (event.target.files.length > 0) {
      let extCheck = this.apiService.typeValidation(event.target.files[0].name.toLowerCase())
      if (!extCheck) {
        return this.apiService.swal(FILEERRORMESSAGE)
      }
      let fileSizeCheck = this.apiService.fileSizeValidation(event.target.files[0].size)
      if (!fileSizeCheck) {
        return this.apiService.swal(FILESIZEERRORMESSAGE)
      }
      
      for (var i = 0; i < event.target.files.length; i++) {
       
        this.files = [...this.files, event.target.files[i]];
      }
     
    }
  }

  apply() {
    // const formData = new FormData();
    // for (var i = 0; i < this.files.length; i++) {
    //   formData.append('proposalVideo', this.files[i]);
    // }
    // formData.append('campaignId', this.id);
    // formData.append('message', this.f.message.value);

    let formData = {
      campaignId :this.id,
      deliveryAddress: this.f.deliveryAddress.value
    }
    this.apiService.postData(`proposals`, formData).subscribe(
      (result: any) => {
        // Handle result
        this.apiService.success('Proposal updated successfully."')
        this.cd.detectChanges();
        this.router.navigate(['/creator/campaigns/list']);
      },
      (error) => {
        // Handle error
        this.apiService.error(error.error);
      },
    );
  }
}

import { Component,ChangeDetectorRef, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-list-review',
  templateUrl: './list-review.component.html',
  styleUrls: ['./list-review.component.scss']
})
export class ListReviewComponent implements OnInit {
  reviews = [];
  loading = false;
  currentRating:number;
  constructor(
    private apiService: ApiService,
    private toastr: ToastrService,
    private cd: ChangeDetectorRef
    ) { }

  ngOnInit(): void {
    this.getReviewList();
  }
  getReviewList(){
    this.apiService.getData("review/reviewList").subscribe(
      (result: any) => {
        
       
        this.reviews = result;
        this.loading=true;
        this.cd.detectChanges();
       });
  }

}

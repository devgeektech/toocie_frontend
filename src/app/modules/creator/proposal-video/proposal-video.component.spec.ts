import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProposalVideoComponent } from './proposal-video.component';

describe('ProposalVideoComponent', () => {
  let component: ProposalVideoComponent;
  let fixture: ComponentFixture<ProposalVideoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProposalVideoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProposalVideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

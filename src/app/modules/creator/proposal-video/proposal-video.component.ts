import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/services/api.service';
import { FILEERRORMESSAGE, FILEEXTENTIONTYPE, FILESIZE, FILESIZEERRORMESSAGE } from 'src/app/constant';

@Component({
  selector: 'app-proposal-video',
  templateUrl: './proposal-video.component.html',
  styleUrls: ['./proposal-video.component.scss']
})
export class ProposalVideoComponent implements OnInit {
  formGroup: FormGroup;
  id:string;
  videos = [];
  loading = false;
  files = [];
  constructor(
    private fb: FormBuilder,
    private apiService: ApiService,
    private toastr: ToastrService,
    private router: Router,
    private route: ActivatedRoute,
    private cd: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    if (this.route.snapshot.params.id != undefined) {
      this.id = this.route.snapshot.params["id"];
    }
    this.getProposalVideoList();
    this.loadForm();
  }

   // convenience getter for easy access to form fields
   get f() {
    return this.formGroup.controls;
  }

  loadForm() {
    this.formGroup = this.fb.group({
      proposalVideo: [''],
    });
  }
  /**
   * Get list of all proposal submitted by creator
   */
  getProposalVideoList(){
    this.apiService.getData(`proposals/${this.id}/video`).subscribe(
      (result: any) => {
        this.videos = result;
        this.loading = true;
        this.cd.detectChanges();
        // Handle result
       });
  }

  onFileChange(event) {
    if (event.target.files.length > 0) {
      let extCheck = this.apiService.typeValidation(event.target.files[0].name.toLowerCase())
      if (!extCheck) {
        return this.apiService.swal(FILEERRORMESSAGE)
      }
      let fileSizeCheck = this.apiService.fileSizeValidation(event.target.files[0].size)
      if (!fileSizeCheck) {
        return this.apiService.swal(FILESIZEERRORMESSAGE)
      }
      
      for (var i = 0; i < event.target.files.length; i++) {
       
        this.files = [...this.files, event.target.files[i]];
      }

      const formData = new FormData();
      for (var i = 0; i < this.files.length; i++) {
        formData.append('proposalVideo', this.files[i]);
      }     
      this.apiService.postDataMultipart(`proposals/${this.id}/uploadVideo`, formData).subscribe(
        (result: any) => {
          // Handle result
          this.apiService.success('Video uploaded successfully."')
          this.cd.detectChanges();
          this.getProposalVideoList();
        },
        (error) => {
          // Handle error
          this.apiService.error(error.error);
        },
      );
     
    }
  }

}

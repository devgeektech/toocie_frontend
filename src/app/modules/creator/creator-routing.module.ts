import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListVideoComponent } from '../brand/video/list-video/list-video.component';
import { ListCampaignComponent } from './campaign/list-campaign/list-campaign.component';
import { ViewCampaignComponent } from './campaign/view-campaign/view-campaign.component';
import { ListProposalComponent } from './proposal/list-proposal/list-proposal.component';
import { ViewProposalComponent } from './proposal/view-proposal/view-proposal.component';
import { ListReviewComponent } from './review/list-review/list-review.component';
import { ListComponent } from './payment/list/list.component';
import { DetailComponent } from './payment/detail/detail.component';
import { ProposalVideoComponent } from './proposal-video/proposal-video.component';

const routes: Routes = [
  {
    path: "campaigns",
    children: [
      {
        path: "list", component: ListCampaignComponent, data: {
          breadcrumb: 'Campaigns'
        }
      },
      {
        path: "detail/:id", component: ViewCampaignComponent, data: {
          breadcrumb: 'Campaign-Detail'
        }
      }
    ],
  },
  {
    path: "proposals",
    children: [
      {
        path: "list", component: ListProposalComponent, data: {
          breadcrumb: 'Proposals'
        }
      },
      {
        path: "detail/:id", component: ViewProposalComponent, data: {
          breadcrumb: 'Proposals-Detail'
        }
      },
      {
        path: ":id", 
        children: [
          {
            path: "video", component: ProposalVideoComponent, data: {
              breadcrumb: 'Videos'
            }
          },
        ],
      }
    ],
  },
  // {
  //   path: "proposal/:id",
  //   children: [
  //     {
  //       path: "video", component: ProposalVideoComponent, data: {
  //         breadcrumb: 'Videos'
  //       }
  //     },
  //   ],
  // },
  {
    path: "reviews",
    children: [
      {
        path: "list", component: ListReviewComponent, data: {
          breadcrumb: 'Reviews'
        }
      },
    ],
  },
  {
    path: "videos",
    children: [
      {
        path: "list", component: ListVideoComponent,
        data: {
          breadcrumb: 'Videos'
        }
      },
    ],
  },
  {
    path: "payments",
    children: [
      {
        path: "list", component: ListComponent,
        data: {
          breadcrumb: 'Payment'
        }
      },
      {
        path: "detail/:id", component: DetailComponent, data: {
          breadcrumb: 'Payment-Detail'
        }
      }
    ],
  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreatorRoutingModule { }

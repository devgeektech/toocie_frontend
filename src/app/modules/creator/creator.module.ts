import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CreatorRoutingModule } from './creator-routing.module';
import { ListCampaignComponent } from './campaign/list-campaign/list-campaign.component';
import { ViewCampaignComponent } from './campaign/view-campaign/view-campaign.component';
import { ListProposalComponent } from './proposal/list-proposal/list-proposal.component';
import { ViewProposalComponent } from './proposal/view-proposal/view-proposal.component';
import { ListReviewComponent } from './review/list-review/list-review.component';
import { NgbModule, NgbRatingModule } from '@ng-bootstrap/ng-bootstrap';
import { NoRecordPlaceholderModule } from '../no-record-placeholder/no-record-placeholder.module';
import { ListComponent } from './payment/list/list.component';
import { DetailComponent } from './payment/detail/detail.component';
import { ProposalVideoComponent } from './proposal-video/proposal-video.component';



@NgModule({
  declarations: [ListCampaignComponent, ViewCampaignComponent, ListProposalComponent, ViewProposalComponent, ListReviewComponent, ListComponent, DetailComponent, ProposalVideoComponent],
  imports: [
    CommonModule,
    CreatorRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbRatingModule,
    NgbModule,
    NoRecordPlaceholderModule
  ]
})
export class CreatorModule { }

import { Component, OnInit, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { timeout } from 'rxjs/operators';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-view-proposal',
  templateUrl: './view-proposal.component.html',
  styleUrls: ['./view-proposal.component.scss']
})
export class ViewProposalComponent implements OnInit {
  formGroup: FormGroup;
  id:string;
  productId:string;
  porposalData:any;
  productName:string;
  loading = false;
  adGoals:any=[];
  adMentions:any=[];
  videoSource:string;
  addressRequired=false;
  @ViewChild('videoPlayer') videoplayer: ElementRef;
  constructor(
    private fb: FormBuilder,
    private apiService: ApiService,
    private toastr: ToastrService,
    private router: Router,
    private route: ActivatedRoute,
    private cd: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    if (this.route.snapshot.params.id != undefined) {
      this.id = this.route.snapshot.params["id"];
      }
    this.fetchById();     
    this.loadForm();   
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.formGroup.controls;
  }

  loadForm() {
    this.formGroup = this.fb.group({
      message: ['', Validators.required],
      deliveryAddress: ['']
    });
  }

  fetchById() {
    this.apiService.getData(`proposals/${this.id}`).subscribe((result) => {
      this.porposalData = result[0];
      this.productId = result[0].campaignRs.productId;
      this.adGoals = result[0].campaignRs.adGoals;
      this.adMentions = result[0].campaignRs.adMentions;
      this.videoSource = result[0].proposalVideo;
      this.addressRequired = result[0].addressRequired;
      if(this.addressRequired===true && result[0].offerAccepeted===false){
        this.formGroup.get("deliveryAddress").setValidators(Validators.required);
        this.formGroup.get("deliveryAddress").updateValueAndValidity();  
      }  
      this.loading=true;
      this.cd.detectChanges();    
    },error =>{
      console.log(error)
    });
  }
  
  toggleVideo() {
    this.videoplayer.nativeElement.play();
  }

  reject() {
    let data ={
      status: 2,
      message:  this.f.message.value
    }
    this.apiService.postData(`proposals/${this.id}/rejectOffer`,data).subscribe(
      (result: any) => {console.log('result', result);
        // Handle result
        console.log("result",result);
       },
      (error) => {
        // Handle error
        this.toastr.error(error.error, "Error!");
        console.log(error.error);
        console.log("error inside");
      },
      () => {
        this.toastr.success("Proposal updated successfully.", "Success!");
        this.cd.detectChanges();
        this.router.navigate(['/creator/proposals/list']);
        // 'onCompleted' callback.
        // No errors, route to new page here
      }
    );
  }

  accept() {
    let data ={
      deliveryAddress: this.f.deliveryAddress.value,
      message:  this.f.message.value
    }
    this.apiService.postData(`proposals/${this.id}/acceptOffer`,data).subscribe(
      (result: any) => {console.log('result', result);
        // Handle result
        console.log("result",result);
       },
      (error) => {
        // Handle error
        this.toastr.error(error.error, "Error!");
        console.log(error.error);
        console.log("error inside");
      },
      () => {
        this.cd.detectChanges();
        this.toastr.success("Proposal updated successfully.", "Success!");
        this.router.navigate(['/creator/proposals/list']);
        // 'onCompleted' callback.
        // No errors, route to new page here
      }
    );
  }


}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrandRoutingModule } from './brand-routing.module';
import { ProductListComponent } from './product/product-list/product-list.component';
import { AddProductComponent } from './product/add-product/add-product.component';
import { AddCampaignComponent } from './campaign/add-campaign/add-campaign.component';
import { ListCampaignComponent } from './campaign/list-campaign/list-campaign.component';
import { NgbModule, NgbNavModule,NgbRatingModule} from '@ng-bootstrap/ng-bootstrap';
import { ListProposalComponent } from './proposal/list-proposal/list-proposal.component';
import { ViewProposalComponent } from './proposal/view-proposal/view-proposal.component';
import { PostProductComponent } from './campaign/post-product/post-product.component';
import { ListVideoComponent } from './video/list-video/list-video.component';
import { NoRecordPlaceholderModule } from '../no-record-placeholder/no-record-placeholder.module';
import { BrandReviewComponent } from './brand-review/brand-review.component';
import { ProposalVideoComponent } from './proposal-video/proposal-video.component';



@NgModule({
  declarations: [ProductListComponent, AddProductComponent, AddCampaignComponent, ListCampaignComponent, ListProposalComponent, ViewProposalComponent, PostProductComponent, ListVideoComponent, BrandReviewComponent, ProposalVideoComponent],
  imports: [
    CommonModule,
    BrandRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgbNavModule,
    NoRecordPlaceholderModule,
    NgbRatingModule
  ]
})
export class BrandModule { }

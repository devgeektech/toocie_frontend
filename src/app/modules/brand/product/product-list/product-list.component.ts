import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
  products = [];
  loading = false;
  pro = []
  constructor(
    private apiService: ApiService,
    private toastr: ToastrService,
    private router: Router,
    private cd: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.getProductList()
  }

  getProductList() {
    this.apiService.getData("products").subscribe(
      (result: any) => {
        this.products = result;
        this.loading = true;
        this.cd.detectChanges();
        // Handle result
      });
  }

  deleteData(val, index) {
    this.apiService.swalConfiramation().then((res: any) => {
      if (res.value) {
        this.apiService.deleteData(`products/${val}`).subscribe(
          (result: any) => {
            this.products.splice(index, 1)
            this.apiService.success('Product has been deleted successfully.')
          },
          (error) => {
            // Handle error
            this.apiService.error(error.error)
          },
        );
      }
    })


  }

}

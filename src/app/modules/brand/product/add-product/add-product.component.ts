import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { ToastrService } from 'ngx-toastr';
import { Observable, of, Subscription } from 'rxjs';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {
  id: number;
  formGroup: FormGroup;
  errorMessage = '';
  hasError: boolean;
  isLoading$: Observable<boolean>;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private apiService: ApiService,
    private toastr: ToastrService,
  ) { }

  ngOnInit(): void {
    if (this.route.snapshot.params.id != undefined) {
      this.id = this.route.snapshot.params["id"];

      this.fetchById();
    }
    this.loadForm();
  }

  get f() {
    return this.formGroup.controls;
  }

  fetchById() {

    this.apiService.getData(`products/${this.id}`).subscribe((result) => {
      this.formGroup.patchValue({
        productName: result.productName,
      });
    });
  }

  loadForm() {

    this.formGroup = this.fb.group({
      productName: ['', Validators.required],
      productUrl:['']
    });
  }

  reset() {
    this.loadForm();
  }

  save() {

    if (!this.formGroup.valid) {
      return;
    }
    const formValues = this.formGroup.value;
    if (this.id) {
      this.edit();
    } else {
      this.create();
    }
  }

  edit() {
    const data = {
      productName: this.f.productName.value,
      productUrl: this.f.productUrl.value
    }
    this.apiService.postData(`products/update/${this.id}`, data).subscribe(
      (result: any) => {
        // Handle result

        this.apiService.success("Product updated successfully.");
        this.router.navigate(['/brand/products/list']);

      },
      (error) => {
        // Handle error
        this.hasError = true;
        this.apiService.error(error.error);

      },

    );
  }

  create() {
    const data = {
      productName: this.f.productName.value,
      productUrl: this.f.productUrl.value
    }
    this.apiService.postData("products", data).subscribe(
      (result: any) => {
        this.apiService.success("Product created successfully.");
        this.router.navigate(['/brand/products/list']);

      },
      (error) => {
        // Handle error
        this.hasError = true;
        this.apiService.error(error.error);
      },

    )
  }


}

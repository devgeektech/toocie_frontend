import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-list-video',
  templateUrl: './list-video.component.html',
  styleUrls: ['./list-video.component.scss']
})
export class ListVideoComponent implements OnInit {
  videos = [];
  loading = false;
  constructor(
    private apiService: ApiService,
    private toastr: ToastrService,
    private cd: ChangeDetectorRef
    ) { }

  ngOnInit(): void {
    this.getVideoList();
  }
  getVideoList(){
    this.apiService.getData("video/videoList").subscribe(
      (result: any) => {
        this.videos = result;
        this.loading=true;
        this.cd.detectChanges();
       });
  }
}

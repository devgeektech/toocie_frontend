import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductListComponent } from './product/product-list/product-list.component';
import { AddProductComponent } from './product/add-product/add-product.component';
import { AddCampaignComponent } from './campaign/add-campaign/add-campaign.component';
import { ListCampaignComponent } from './campaign/list-campaign/list-campaign.component';
import { ListProposalComponent } from './proposal/list-proposal/list-proposal.component';
import { ViewProposalComponent } from './proposal/view-proposal/view-proposal.component';
import { PostProductComponent } from './campaign/post-product/post-product.component';
import { ListVideoComponent } from './video/list-video/list-video.component';
import { BrandReviewComponent } from './brand-review/brand-review.component';
import { ProposalVideoComponent } from './proposal-video/proposal-video.component';

const routes: Routes = [
  {
    path: "products",
    children: [
      {
        path: "list",
        component: ProductListComponent,
        data: {
          breadcrumb: 'Products'
        }
      },
      {
        path: "add", component: AddProductComponent,
        data: {
          breadcrumb: 'Add Product'
        }
      },
      {
        path: "edit/:id", component: AddProductComponent,
        data: {
          breadcrumb: 'Update Product'
        }
      },
    ],
  },
  {
    path: "review",
    children: [
      {
        path: "list",
        component: BrandReviewComponent,
        data: {
          breadcrumb: 'Review'
        }
      },
     
    ],
  },
  {
    path: "videos",
    children: [
      {
        path: "list", component: ListVideoComponent,
        data: {
          breadcrumb: 'Videos'
        }
      },
    ],
  },
  {
    path: "campaigns",
    children: [
      {
        path: "list", component: ListCampaignComponent,
        data: {
          breadcrumb: 'Campaigns'
        }
      },
      {
        path: "add", component: AddCampaignComponent, data: {
          breadcrumb: 'Add Campaign'
        }
      },
      {
        path: "edit/:id", component: AddCampaignComponent, data: {
          breadcrumb: 'Update Campaign'
        }
      },
      {
        path: ":campaignId/:productId", component: PostProductComponent, data: {
          breadcrumb: 'Post Campaign'
        }
      },
    ],
  },
  {
    path: "proposals",
    children: [
      {
        path: "list", component: ListProposalComponent, data: {
          breadcrumb: 'Proposals'
        }
      },
      {
        path: "detail/:id", component: ViewProposalComponent, data: {
          breadcrumb: 'Proposal-detail'
        }
      },
    ],
  },
  {
    path: "proposal/:id",
    children: [
      {
        path: "video", component: ProposalVideoComponent, data: {
          breadcrumb: 'Videos'
        }
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BrandRoutingModule { }

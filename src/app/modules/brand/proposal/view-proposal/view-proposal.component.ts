import { Component, OnInit, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-view-proposal',
  templateUrl: './view-proposal.component.html',
  styleUrls: ['./view-proposal.component.scss']
})
export class ViewProposalComponent implements OnInit {
  formGroup: FormGroup;
  id: string;
  productId: string;
  porposalData: any;
  productName: string;
  loading = false;
  adGoals: any = [];
  adMentions: any = [];
  videoSource: string;
  readOnly = false;
  @ViewChild('videoPlayer') videoplayer: ElementRef;
  constructor(
    private fb: FormBuilder,
    private apiService: ApiService,
    private toastr: ToastrService,
    private router: Router,
    private route: ActivatedRoute,
    private cd: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    if (this.route.snapshot.params.id != undefined) {
      this.id = this.route.snapshot.params["id"];
    }
    this.fetchById();
    this.loadForm();
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.formGroup.controls;
  }

  loadForm() {
    this.formGroup = this.fb.group({
      message: ['', Validators.required]
    });
  }

  fetchById() {
    this.apiService.getData(`proposals/${this.id}`).subscribe((result) => {
      this.porposalData = result[0];
      this.productId = result.productId;
      this.adGoals = result.adGoals;
      this.adMentions = result.adMentions;
      this.videoSource = result[0].proposalVideo;
      this.loading = true;
      if(result[0].status==1 || result[0].status==2){
        this.readOnly = true;
      }
      this.formGroup.patchValue({
        message:result[0].message
      })  
     
      this.cd.detectChanges();
    }, error => {
      console.log(error)
    });
  }

  toggleVideo() {
    this.videoplayer.nativeElement.play();
  }

  approve() {
    let data = {
      status: 1,
      message: this.f.message.value
    }
    
    this.apiService.postData(`proposals/${this.id}/sendOffer`, data).subscribe(
      (result: any) => {
        console.log('result', result);
        // Handle result
        this.apiService.success("Proposal updated successfully.");
        this.cd.detectChanges();
        this.router.navigate(['/brand/proposals/list']);
      },
      (error) => {
        // Handle error
        this.apiService.error(error.error)
      },
    );
  }

  reject() {
    this.apiService.postData(`proposals/${this.id}/updateStatus`, { status: 2 }).subscribe(
      (result: any) => {
        console.log('result', result);
        // Handle result
        this.cd.detectChanges();
        this.apiService.success("Proposal updated successfully.");

        this.router.navigate(['/brand/proposals/list']);
      },
      (error) => {
        // Handle error
        this.apiService.error(error.error)
      },

    );
  }

}

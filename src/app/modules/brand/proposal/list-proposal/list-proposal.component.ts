import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-proposal',
  templateUrl: './list-proposal.component.html',
  styleUrls: ['./list-proposal.component.scss']
})
export class ListProposalComponent implements OnInit {
  proposals = [];
  loading = false;
  constructor(
    private apiService: ApiService,
    private toastr: ToastrService,
    private router: Router,
    private cd: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.getProposalList();
  }

  /**
   * Get list of all proposal submitted by creator
   */
  getProposalList(){
    this.apiService.getData("proposals").subscribe(
      (result: any) => {
        this.proposals = result;
        this.loading = true;
        this.cd.detectChanges();
        // Handle result
       });
  }
  markasApproved(id){
      this.apiService.getData(`proposals/updatecampaignStatus/${id}`).subscribe((result) => {
        console.log(result)
      })
    
  }
}

import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/services/api.service';
import Swal from "sweetalert2";
import { FILEERRORMESSAGE, FILEEXTENTIONTYPE, FILESIZE, FILESIZEERRORMESSAGE } from 'src/app/constant';


@Component({
  selector: 'app-proposal-video',
  templateUrl: './proposal-video.component.html',
  styleUrls: ['./proposal-video.component.scss']
})
export class ProposalVideoComponent implements OnInit {
  formGroup: FormGroup;
  id:string;
  videos = [];
  loading = false;
  files = [];
  constructor(
    private fb: FormBuilder,
    private apiService: ApiService,
    private toastr: ToastrService,
    private router: Router,
    private route: ActivatedRoute,
    private cd: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    if (this.route.snapshot.params.id != undefined) {
      this.id = this.route.snapshot.params["id"];
    }
    this.getProposalVideoList();
  }


  /**
   * Get list of all proposal submitted by creator
   */
  getProposalVideoList(){
    this.apiService.getData(`proposals/${this.id}/video`).subscribe(
      (result: any) => {
        this.videos = result;
        this.loading = true;
        this.cd.detectChanges();
        // Handle result
       });
  }

  updateStatus(videoId,status){
    if(status =="2"){
      Swal.fire({
        title: 'Reason for rejection',
        input: 'text',
        inputAttributes: {
          autocapitalize: 'off'
        },
        showCancelButton: true,
        confirmButtonText: 'Reject',
        showLoaderOnConfirm: true,
        confirmButtonColor: '#1BC5BD',
        cancelButtonColor: '#F64E60', 
        customClass:{
          title:"rejection-title"
        },
        preConfirm: (rejectionReason) => {
         
        },
        allowOutsideClick: () => !Swal.isLoading()
      }).then((result) => {
        console.log("aaa",result);
        if (result.isConfirmed) {
          this.apiService.postData(`video/updateVideoStatus`,{videoId:videoId,status:status,rejectionReason:result.value}).subscribe(
            (res) => {
              this.apiService.success("Video status has been updated successfully.");
              this.cd.detectChanges();
              this.getProposalVideoList();
            },
            (error) => {
              console.log(error);
            }
          );
        }
      })
    }
    if(status =="1"){
      //alert("id"+id+"<==>status=>"+status);
    this.apiService.postData(`video/updateVideoStatus`,{videoId:videoId,status:status}).subscribe(
      (res) => {
        this.apiService.success("Video status has been updated successfully.");
        this.cd.detectChanges();
        this.getProposalVideoList();
      },
      (error) => {
        console.log(error);
      }
    );
    }
    
    
  }

}

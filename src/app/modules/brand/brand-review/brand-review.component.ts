import { Component, OnInit,ChangeDetectorRef} from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-brand-review',
  templateUrl: './brand-review.component.html',
  styleUrls: ['./brand-review.component.scss']
})
export class BrandReviewComponent implements OnInit {
  reviews = [];
  constructor(private apiService: ApiService,
    private toastr: ToastrService,
    private cd: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.getbrandReviewList();
  }
  getbrandReviewList(){
    this.apiService.getData("review/brandreviewList").subscribe(
      (result: any) => {
        console.log("resultadmin",result);
       
        this.reviews = result;
        this.cd.detectChanges();
       });
  }
}

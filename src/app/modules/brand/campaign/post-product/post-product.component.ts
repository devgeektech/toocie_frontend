import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-post-product',
  templateUrl: './post-product.component.html',
  styleUrls: ['./post-product.component.scss']
})
export class PostProductComponent implements OnInit {
  campaignId: number;
  productId: number;
  campaignData:any;
  productData:any;
  formGroup: FormGroup;
  errorMessage = '';
  hasError : boolean;
  loading = false;
  proposalData:any;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private apiService: ApiService,
    private toastr: ToastrService,
    private cd: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    if (this.route.snapshot.params.campaignId != undefined && this.route.snapshot.params.productId != undefined) {
      this.campaignId = this.route.snapshot.params["campaignId"];
      this.productId = this.route.snapshot.params["productId"];  
      this.fetchCampaignDetail();
      this.getProductDetails();
      this.getProposalDetails();
      }
    this.loadForm();
  }

  loadForm() {
    this.formGroup = this.fb.group({
      courierVendorName: ['', Validators.required],
      trackingNumber: ['', Validators.required]
    });
  }

  get f() {
    return this.formGroup.controls;
  }

  fetchCampaignDetail() {    
    this.apiService.getData(`campaigns/${this.campaignId}`).subscribe((result) => {      
      this.campaignData = result;
      this.formGroup.patchValue({
        courierVendorName: result.courierVendorName,
        trackingNumber: result.trackingNumber
      });
      this.loading = true;
      this.cd.detectChanges();
    });
  }
  getProductDetails() {
    this.apiService.getData(`products/${this.productId}`).subscribe((result) => {
     this.productData = result;     
     this.cd.detectChanges();
    });
  }

  getProposalDetails() {
    this.apiService.getData(`campaigns/${this.campaignId}/getAcceptedProposal`).subscribe((result) => {
     this.proposalData = result;
     this.cd.detectChanges();
    });
  }
  

  reset() {
    this.loadForm();
  }

  save() {
    
    if (!this.formGroup.valid) {
      return;
    }
    const data = {
      productId:this.productId,
      campaignId:this.campaignId,
      courierVendorName:this.f.courierVendorName.value,
      trackingNumber:this.f.trackingNumber.value
    }
    this.apiService.postData("products/sendToCreator", data).subscribe(
      (result: any) => {console.log('result', result);
        // Handle result
        console.log("result",result);
       },
      (error) => {
        // Handle error
        this.hasError = true;
        this.toastr.error(error.error, "Error!");
        console.log(error.error);
        console.log("error inside");
      },
      () => {
        this.toastr.success("Courier deatil saved successfully.", "Success!");
        this.router.navigate(['/brand/campaigns/list']);
        // 'onCompleted' callback.
        // No errors, route to new page here
      }
    );
  }

}

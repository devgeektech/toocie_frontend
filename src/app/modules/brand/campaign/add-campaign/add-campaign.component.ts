import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { ToastrService } from 'ngx-toastr';
import { Observable, of, Subscription } from 'rxjs';
import { COUNTRIES } from "src/app/country"
import { Country } from 'country-state-city';

@Component({
  selector: 'app-add-campaign',
  templateUrl: './add-campaign.component.html',
  styleUrls: ['./add-campaign.component.scss']
})
export class AddCampaignComponent implements OnInit {
  activePlatform:any;
  activeVideoFormat:any;
  products: [];
  countryList = Country.getAllCountries();;
  id: number;
  balance: number;
  formGroup: FormGroup;
  errorMessage = '';
  hasError: boolean;
  loading = false;
  activeTab = '';
  tags = [];
  tagIds: any = [];
  ageGroups: any = [];
  adGoals = [
    {
      value: '',
    },
    {
      value: '',
    },
    {
      value: '',
    }
  ];
  adMentions = [
    {
      value: '',
    },
    {
      value: '',
    },
    {
      value: '',
    },
    {
      value: '',
    },
    {
      value: '',
    }
  ];
  ageGroup = [
    {
      value: '1',
      label: '18-24',
      selected: false
    },
    {
      value: '2',
      label: '25-34',
      selected: false
    },
    {
      value: '3',
      label: '35-44',
      selected: false
    },
    {
      value: '4',
      label: '45+',
      selected: false
    }
  ];
  selectedAgeGroup = [];
  selectedTags = [];
  checkedValue: [];
  isLoading$: Observable<boolean>;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private apiService: ApiService,
    private toastr: ToastrService,
    private cd: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.activePlatform = 'Facebook';
    this.activeVideoFormat = "1:1";
    if (this.route.snapshot.params.id != undefined) {
      this.id = this.route.snapshot.params["id"];

      this.fetchById();
    }
    this.getProductList();
    this.getTagsList();

    this.loadForm();
  }

  get f() {
    return this.formGroup.controls;
  }

  fetchById() {
    this.apiService.getData(`campaigns/${this.id}`).subscribe((result) => {
      this.adGoals = result.adGoals;

      this.selectedTags = result.tags;
      this.selectedAgeGroup = result.ageGroup;

      this.adMentions = result.adMentions;
      this.activePlatform = result.displayPlatForm;
      this.activeVideoFormat = result.videoFormat;
      this.formGroup.patchValue({
        campaignName: result.campaignName,
        productId: result.productId,
        gender: result.gender,
        location: result.location
      });
    });
  }

  addGoal() {
    this.adGoals = [...this.adGoals, { value: '' }];
  }

  removeGoal(index) {
    this.adGoals.splice(index, 1);
  }

  addMention() {
    this.adMentions = [...this.adMentions, { value: '' }];
  }

  removeMention(index) {
    this.adMentions.splice(index, 1);
  }

  changeTag(event) {
    const value = event.target.value;
    const index = this.selectedTags.findIndex(x => x._id == value);

    if (index > -1) this.selectedTags.splice(index, 1);
    else {
      const index = this.tags.findIndex(x => x._id == value);
      this.selectedTags = [...this.selectedTags, this.tags[index]];
    }
    // console.log("val",val.currentTarget.value);
    // this.formGroup.get('tagId').setValue(val.currentTarget.value);
    // this.tagIds = [...this.tagIds,val.currentTarget.value];
  }

  removeTag(index) {
    this.selectedTags.splice(index, 1);
  }

  loadForm() {

    this.formGroup = this.fb.group({
      campaignName: ['', Validators.required],
      //campaignCost: ['', Validators.required],
      campaignCost: [''],
      productId: ['', Validators.required],
      gender: ['', Validators.required],

      //ageGroup:['',Validators.required],
      location: ['', Validators.required],
    });

  }

  reset() {
    this.loadForm();
  }

  save() {
    if (!this.formGroup.valid) {
      return;
    }
    const formValues = this.formGroup.value;
    if (this.id) {
      this.edit();
    } else {
      this.create();
    }
  }

  edit() {
    const data = {
      campaignName: this.f.campaignName.value,
      productId: this.f.productId.value,
      adGoals: this.adGoals,
      adMentions: this.adMentions,
      gender: this.f.gender.value,
      ageGroup: this.selectedAgeGroup,
      location: this.f.location.value,
      tags: this.selectedTags,
      displayPlatForm:this.activePlatform,
      videoFormat:this.activeVideoFormat
      //campaignCost: this.f.campaignCost.value
    }
    this.apiService.postData(`campaigns/${this.id}`, data).subscribe(
      (result: any) => {
        // Handle result
        this.router.navigate(['/brand/campaigns/list']);
        this.apiService.success("Campaign created successfully.")
      },
      (error) => {
        // Handle error
        this.hasError = true;
        this.apiService.error(error.error)

      },

    );
  }

  async create() {
    const data = {
      campaignName: this.f.campaignName.value,
      productId: this.f.productId.value,
      adGoals: this.adGoals,
      adMentions: this.adMentions,
      gender: this.f.gender.value,
      ageGroup: this.selectedAgeGroup,
      location: this.f.location.value,
      tags: this.selectedTags,
      campaignCost: this.f.campaignCost.value,
      displayPlatForm:this.activePlatform,
      videoFormat:this.activeVideoFormat
    }
    this.apiService.postData("campaigns", data).subscribe(
      async (result: any) => {
        // Handle result
        await this.getBalance();
        this.router.navigate(['/brand/campaigns/list']);
        this.apiService.success("Campaign created successfully.")
      },
      (error) => {
        // Handle error
        this.hasError = true;
        this.apiService.error(error.error)
      },
    );
  }

  async getBalance() {
    this.apiService.getData("auth/balance").subscribe(
      (result: any) => {
        this.balance = result.balance;
        const retrievedString = localStorage.getItem("ugcUserobject");
        const parsedObject = JSON.parse(retrievedString);
        parsedObject.balance = result.balance;
        const strigifiedForStorage = JSON.stringify(parsedObject);
        localStorage.setItem("ugcUserobject", strigifiedForStorage);
        this.apiService.SharingData.next(parsedObject.balance);
        this.cd.detectChanges();
        // Handle result
      });
  }

  getProductList() {
    this.apiService.getData("products").subscribe(
      (result: any) => {
        this.products = result;
        this.loading = true;
        this.cd.detectChanges();
        // Handle result
      });
  }

  getTagsList() {
    this.apiService.getData("tags").subscribe(
      (result: any) => {
        this.tags = result;
        this.loading = true;
        this.cd.detectChanges();
        // Handle result
      });
  }

changeProduct(event){

}

  changeAgeGroup(value) {
    const index = this.selectedAgeGroup.indexOf(value);

    // console.log('index', index);
    // console.log('this.selectedAgeGroup', this.selectedAgeGroup);
    // return false;
    if (index > -1) this.selectedAgeGroup.splice(index, 1);
    else this.selectedAgeGroup = [...this.selectedAgeGroup, value];

    console.log(this.selectedAgeGroup);
  }


  getScrollTo(id: string) {
    this.activeTab = id
    const element = this.getElementById(id)

  }

  getElementById(id: string): HTMLElement {
    const element = <HTMLElement>document.querySelector(`#${id}`);
    return element;
  }

//   ngAfterViewInit(){
//     // print array of CustomComponent objects
//     console.log("hello",this.platform);
// }

  choosePlatform(event){
    this.activePlatform = event;
    console.log("event",event);
  }
  chooseVideoFormat(event){
    this.activeVideoFormat = event;
  }

}

import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-email-verification',
  templateUrl: './email-verification.component.html',
  styleUrls: ['./email-verification.component.scss']
})
export class EmailVerificationComponent implements OnInit {
  email: string;
  code: string;
  hasError=false;
  hasSuccess=false;
  constructor(
    private route: ActivatedRoute,
    private apiService: ApiService,
    private toastr: ToastrService,
    private cd: ChangeDetectorRef,
  ) { }

  ngOnInit(): void {

    if (this.route.snapshot.params.email != undefined && this.route.snapshot.params.code != undefined) {
      this.email = this.route.snapshot.params["email"];
      this.code = this.route.snapshot.params["code"];
      this.fetchById();
      }

  }
  /**
   * Check and verify user.
   * @param {string} email
   * @param {string} code
   * @param {object} result - return verified user detail if link is valid
   */
  fetchById() {    
    this.apiService.getData(`auth/emailVerification/${this.email}/${this.code}`).subscribe(
      (result: any) => {
        // Handle result
        this.cd.detectChanges();
       },
      (error) => {
        this.hasError= true;
        this.cd.detectChanges();
        // Handle error
        this.toastr.error(error.error, "Error!");
      },
      () => {
        this.hasSuccess= true;
        this.cd.detectChanges();
        this.toastr.success("Your account is successfully verified.", "Success!");
        
      }
    );
  }
}

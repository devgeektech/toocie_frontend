import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
import { Observable } from 'rxjs/index';

@Injectable({
  providedIn: 'root',
})
export class AuthService implements CanActivate {
  constructor(private readonly router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.isAuthenticated();
  }

  public isAuthenticated(): boolean {
    const token = localStorage.getItem('authorization');
    if (token === null) {
      this.router.navigate(['/auth/login']);
      return false;
    }
    return true;
  }
}

import { Injectable } from '@angular/core';
import {
  HttpResponse,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { finalize } from 'rxjs/operators';
import { environment } from '../../environments/environment'
@Injectable()
export class LoaderInterceptorService implements HttpInterceptor {
  serverUrl = environment.BaseUrl
 
  constructor(public loaderService: NgxUiLoaderService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {


      this.loaderService.start();

      return next.handle(req).pipe(
        finalize(() => this.loaderService.stop())
      );
    }
  }



import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Subject } from 'rxjs';
import { ToastrService } from "ngx-toastr";
import Swal from 'sweetalert2';
import { FILEERRORMESSAGE, FILEEXTENTIONTYPE, FILESIZE, FILESIZEERRORMESSAGE } from 'src/app/constant';

@Injectable({
  providedIn: "root",
})
export class ApiService {
  public jwt = "";
  public BaseUrl = environment.BaseUrl;
  SharingData = new Subject();

  constructor(private http: HttpClient, private tostr: ToastrService) {
    this.jwt = localStorage.getItem("authorization");
  }

  swalConfiramation() {
    return Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#6993FF',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'

    })
  }
  error(message) {
    this.tostr.error(message, 'Error!', {
      timeOut: 1200,
    })
  }

  success(message) {
    this.tostr.success(message, 'Success!', {
      timeOut: 1200,
    })

  }

  fileSizeValidation(size: number) {

    if (size > FILESIZE
    ) {
      return false
    }
    else {
      return true
    }
  }

  typeValidation(name: string) {
    let ext = name.substring(name.lastIndexOf('.') + 1);
    const extType = FILEEXTENTIONTYPE
    let res = extType.includes(ext)
    return res

  }


  swal(message) {
    return Swal.fire({
      icon: 'error',
      confirmButtonColor: '#6993FF',
      text: message,
    })
  }



  getToken() {
    let token = localStorage.getItem("authorization") ? localStorage.getItem("authorization") : "";
    return token;
  }
  postData(url: string, data) {

    const headers = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: this.getToken(),
      }),
    };

    return this.http.post(this.BaseUrl + url, data, headers);
  }

  postDataMultipart(url: string, data) {
    const headers = {
      headers: new HttpHeaders({
        Authorization: this.getToken(),
      }),
    };

    return this.http.post(this.BaseUrl + url, data, headers);
  }

  putData(url: string, data) {
    const headers = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: this.getToken(),
      }),
    };

    return this.http.put<any>(this.BaseUrl + url, data, headers);
  }
  getData(url: string) {
    const headers = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: this.getToken(),
      }),
    };
    return this.http.get<any>(this.BaseUrl + url, headers);
  }

  deleteData(url: string) {
    const headers = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: this.getToken(),
      }),
    };
    return this.http.delete<any>(this.BaseUrl + url, headers);
  }

 
}



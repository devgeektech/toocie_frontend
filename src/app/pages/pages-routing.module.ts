import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommunicationComponent } from './_layout/components/communication/communication.component';
import { LayoutComponent } from './_layout/layout.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: 'admin',
        loadChildren: () =>
          import('../modules/admin/admin.module').then((m) => m.AdminModule),
      },
      {
        path: 'brand',
        loadChildren: () =>
          import('../modules/brand/brand.module').then((m) => m.BrandModule),
      },

      {
        path: 'review',
        loadChildren: () =>
          import('../modules/review-creator/review-creator.module').then((m) => m.ReviewCreatorModule),
      },
      {
        path: 'transactions',
        loadChildren: () =>
          import('../modules/transaction/transaction.module').then((m) => m.TransactionModule),
      },
      {
        path: 'creator',
        loadChildren: () =>
          import('../modules/creator/creator.module').then((m) => m.CreatorModule),
      },
      {
        path: 'dashboard',
        loadChildren: () =>
          import('./dashboard/dashboard.module').then((m) => m.DashboardModule),
      },
      {
        path: 'new-dashboard',
        loadChildren: () =>
          import('./dashboard/new-dashboard/new-dashboard.module').then((m) => m.NewDashboardModule),
      },
      {
        path: 'builder',
        loadChildren: () =>
          import('./builder/builder.module').then((m) => m.BuilderModule),
      },
      {
        path: 'ecommerce',
        loadChildren: () =>
          import('../modules/e-commerce/e-commerce.module').then(
            (m) => m.ECommerceModule
          ),
      },
      {
        path: 'user-management',
        loadChildren: () =>
          import('../modules/user-management/user-management.module').then(
            (m) => m.UserManagementModule
          ),
      },
      {
        path: 'user-profile',
        loadChildren: () =>
          import('../modules/user-profile/user-profile.module').then(
            (m) => m.UserProfileModule
          ),
      },
      {
        path: 'ngbootstrap',
        loadChildren: () =>
          import('../modules/ngbootstrap/ngbootstrap.module').then(
            (m) => m.NgbootstrapModule
          ),
      },
      {
        path: 'wizards',
        loadChildren: () =>
          import('../modules/wizards/wizards.module').then(
            (m) => m.WizardsModule
          ),
      },
      {
        path: 'material',
        loadChildren: () =>
          import('../modules/material/material.module').then(
            (m) => m.MaterialModule
          ),
      },



      {
        path: 'communication',
        component: CommunicationComponent
        , data: {
          breadcrumb: 'Communication'
        }
      },
      {
        path: '',
        redirectTo: 'dashboard',
        
        pathMatch: 'full',
      },
      {
        path: '**',
        redirectTo: 'error/404',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule { }

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-outer-header',
  templateUrl: './outer-header.component.html',
  styleUrls: ['./outer-header.component.scss']
})
export class OuterHeaderComponent implements OnInit {
  userName =  '';
  constructor() { }

  ngOnInit(): void {
    let userObject: any = localStorage.getItem('ugcUserobject');
    
    if(userObject != undefined){
      console.log('userObject', userObject);
      userObject = JSON.parse(userObject);
      this.userName = userObject.firstName;
    }
  }

}

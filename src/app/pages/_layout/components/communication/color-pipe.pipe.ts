import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'colorPipe'
})
export class ColorPipePipe implements PipeTransform {

  transform(index: number): any {
    switch (index) {
      case 0: return "darkolivegreen"
      case 1: return "cadetblue"
      case 2: return "coral"
      // case 3: return "brown"
      // case 4: return "darkslateblue"
      // case 5: return "darkslategray"
      // case 6: return "indianred"
      // case 7: return "darkgoldenrod"
      // case 8: return "lightseagreen"
      // case 9: return "crimson"
      // case 10: return "steelblue"


      default: return "darkgray"
    }


  }

}

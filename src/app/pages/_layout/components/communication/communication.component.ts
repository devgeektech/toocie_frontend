import {
  Component,
  OnInit,
  ChangeDetectorRef,
  ViewChild,
  ElementRef,
  ViewChildren,
  QueryList,
} from "@angular/core";
import { ApiService } from "src/app/services/api.service";
import { ToastrService } from "ngx-toastr";
import { Router } from "@angular/router";
import {
  FILEERRORMESSAGE,
  FILEEXTENTIONTYPE,
  FILESIZE,
  FILESIZEERRORMESSAGE,
} from "src/app/constant";
import Swal from "sweetalert2";
import { saveAs } from "file-saver/src/FileSaver";

@Component({
  selector: "app-communication",
  templateUrl: "./communication.component.html",
  styleUrls: ["./communication.component.scss"],
})
export class CommunicationComponent implements OnInit {
  @ViewChild("scrollBottom") private scrollBottom: ElementRef;

  userArr = [];
  receiverId;
  message: any = "";
  senderId: any = "";
  messages = [];
  receiverName = "";
  userName;
  receiverNameLogo = "";
  recivercolor;
  colorArr = [];
  index;
  userType;
  videoRes;
  constructor(
    private router: Router,
    private apiService: ApiService,
    private cd: ChangeDetectorRef,
    private tostr: ToastrService
  ) {}

  ngOnInit(): void {
    this.getUsers();

    let authObject: any = localStorage.getItem("ugcUserobject");
    authObject = JSON.parse(authObject);
    this.senderId = authObject.userId;
    this.userName = this.setName(authObject.firstName);
    this.userType = authObject.userType;
  }

  navigate(id) {
    this.receiverId = id;
    this.router.navigate(["communication"], { queryParams: { id: id } });
  }

  getUsers() {
    this.apiService.getData("chat/getUsers").subscribe(
      (res: any) => {
        this.userArr = res;
        if (res.length != 0) {
          this.recivercolor = this.colorArr[0];

          //this.receiverId = res[0]._id;

          // this.router.navigate(["communication"], {
          //   queryParams: { id: res[0]._id },
          // });
          //this.receiverName = `${res[0].firstName} ${res[0].lastName}`;
          //this.receiverNameLogo = this.setName(res[0].firstName);
          // this.getUserDetail(this.receiverId);
        }
        this.cd.detectChanges();
      },
      (error) => {
        console.log(error);
      }
    );
  }

  getUserDetail(id, firstName?, lastName?, i?) {
    let name = firstName;

    this.navigate(id);
    if (firstName) {
      this.receiverNameLogo = this.setName(firstName);
      this.receiverName = `${firstName} ${lastName}`;
    }

    this.apiService.getData(`chat/getUserDetail/${this.receiverId}`).subscribe(
      (res: any) => {
        this.messages = res;
        this.getUsers();
        this.scrollToError();
        this.cd.detectChanges();
      },
      (error) => {
        console.log(error);
      }
    );
  }

  addMessage() {
    if(!this.message){ return }
    let obj = {
      receiverId: this.receiverId,
      message: this.message,
    };
    this.apiService.postData("chat", obj).subscribe(
      (res: any) => {
        this.message = "";
        this.getUserDetail(this.receiverId);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  saveTextFile(file) {
    let fileType = file.substring(file.lastIndexOf(".") + 1);
    console.log(fileType);
    var blob = new Blob([file], {
      type: `video/${fileType}`,
    });
    saveAs(blob, "file");
  }

  scrollToError(): void {
    setTimeout(() => {
      let firstElementWithError;
      firstElementWithError = document.querySelector(".scrollBottom");
      firstElementWithError.scrollIntoView({
        behavior: "smooth",
        block: "center",
      });
    }, 0);
  }

  changeColor(i) {
    this.index = i;
    let color;
    let randomColor = "#" + Math.floor(Math.random() * 16777215).toString(16);
    this.colorArr.push(randomColor);
    color = this.colorArr[i];
    return color;
  }

  onChange(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];

      let extCheck = this.apiService.typeValidation(file.name.toLowerCase());
      if (!extCheck) {
        return this.apiService.swal(FILEERRORMESSAGE);
      }

      let fileSizeCheck = this.apiService.fileSizeValidation(file.size);
      if (!fileSizeCheck) {
        return this.apiService.swal(FILESIZEERRORMESSAGE);
      }
      const formData = new FormData();
      formData.append("video", file);
      formData.append("receiverId", this.receiverId);
      this.apiService.postDataMultipart("video", formData).subscribe(
        (data: any) => {
          this.getUserDetail(this.receiverId);
        },
        (error) => {
          console.log(error);
        }
      );
    } else {
    }
  }

  approveVideo(id, i) {
    this.apiService.getData(`video/updateStatus/${id}`).subscribe(
      (res) => {
        this.messages[i].isApproved = true;
        this.apiService.success("Video has been approved successfully.");

        this.cd.detectChanges();
      },
      (error) => {
        console.log(error);
      }
    );
  }

  setName(name) {
    let userName = name.split("");
    return userName[0];
  }
}

import {
  Component,
  OnInit,
  AfterViewInit,
  Input,
  ChangeDetectorRef,
} from "@angular/core";
import { Observable } from "rxjs";
import { LayoutService } from "../../../../_metronic/core";
import { AuthService } from "../../../../modules/auth/_services/auth.service";
import { UserModel } from "../../../../modules/auth/_models/user.model";
import KTLayoutQuickSearch from "../../../../../assets/js/layout/extended/quick-search";
import KTLayoutQuickNotifications from "../../../../../assets/js/layout/extended/quick-notifications";
import KTLayoutQuickActions from "../../../../../assets/js/layout/extended/quick-actions";
import KTLayoutQuickCartPanel from "../../../../../assets/js/layout/extended/quick-cart";
import KTLayoutQuickPanel from "../../../../../assets/js/layout/extended/quick-panel";
import KTLayoutQuickUser from "../../../../../assets/js/layout/extended/quick-user";
import KTLayoutHeaderTopbar from "../../../../../assets/js/layout/base/header-topbar";
import { KTUtil } from "../../../../../assets/js/components/util";
import { ApiService } from "src/app/services/api.service";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
@Component({
  selector: "app-topbar",
  templateUrl: "./topbar.component.html",
  styleUrls: ["./topbar.component.scss"],
})
export class TopbarComponent implements OnInit, AfterViewInit {
  userType: string;
  userName: string;
  balance: any;
  user$: Observable<UserModel>;
  // tobbar extras
  extraSearchDisplay: boolean;
  extrasSearchLayout: "offcanvas" | "dropdown";
  extrasNotificationsDisplay: boolean;
  extrasNotificationsLayout: "offcanvas" | "dropdown";
  extrasQuickActionsDisplay: boolean;
  extrasQuickActionsLayout: "offcanvas" | "dropdown";
  extrasCartDisplay: boolean;
  extrasCartLayout: "offcanvas" | "dropdown";
  extrasQuickPanelDisplay: boolean;
  extrasLanguagesDisplay: boolean;
  extrasUserDisplay: boolean;
  extrasUserLayout: "offcanvas" | "dropdown";

  constructor(
    private layout: LayoutService,
    private toaster: ToastrService,
    private auth: AuthService,
    private apiService: ApiService,
    private cd: ChangeDetectorRef,
    private router: Router
  ) {
    this.apiService.SharingData.subscribe((res: any) => {
      this.balance = res;
    });
    this.user$ = this.auth.currentUserSubject.asObservable();
  }

  ngOnInit(): void {
    // topbar extras
    this.extraSearchDisplay = this.layout.getProp("extras.search.display");
    this.extrasSearchLayout = this.layout.getProp("extras.search.layout");
    this.extrasNotificationsDisplay = this.layout.getProp(
      "extras.notifications.display"
    );
    this.extrasNotificationsLayout = this.layout.getProp(
      "extras.notifications.layout"
    );
    this.extrasQuickActionsDisplay = this.layout.getProp(
      "extras.quickActions.display"
    );
    this.extrasQuickActionsLayout = this.layout.getProp(
      "extras.quickActions.layout"
    );
    this.extrasCartDisplay = this.layout.getProp("extras.cart.display");
    this.extrasCartLayout = this.layout.getProp("extras.cart.layout");
    this.extrasLanguagesDisplay = this.layout.getProp(
      "extras.languages.display"
    );
    this.extrasUserDisplay = this.layout.getProp("extras.user.display");
    this.extrasUserLayout = this.layout.getProp("extras.user.layout");
    this.extrasQuickPanelDisplay = this.layout.getProp(
      "extras.quickPanel.display"
    );

    const userDetail = JSON.parse(localStorage.getItem("ugcUserobject"));
    this.userName = `${userDetail.firstName} ${userDetail.lastName}`;
    this.userType = userDetail.userType;
    this.balance = userDetail.balance;
    // this.balance = userDetail.balance?userDetail.balance:0;
  }

  ngAfterViewInit(): void {
    KTUtil.ready(() => {
      // Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
      // Add 'implements AfterViewInit' to the class.
      if (this.extraSearchDisplay && this.extrasSearchLayout === "offcanvas") {
        KTLayoutQuickSearch.init("kt_quick_search");
      }

      if (
        this.extrasNotificationsDisplay &&
        this.extrasNotificationsLayout === "offcanvas"
      ) {
        // Init Quick Notifications Offcanvas Panel
        KTLayoutQuickNotifications.init("kt_quick_notifications");
      }

      if (
        this.extrasQuickActionsDisplay &&
        this.extrasQuickActionsLayout === "offcanvas"
      ) {
        // Init Quick Actions Offcanvas Panel
        KTLayoutQuickActions.init("kt_quick_actions");
      }

      if (this.extrasCartDisplay && this.extrasCartLayout === "offcanvas") {
        // Init Quick Cart Panel
        KTLayoutQuickCartPanel.init("kt_quick_cart");
      }

      if (this.extrasQuickPanelDisplay) {
        // Init Quick Offcanvas Panel
        KTLayoutQuickPanel.init("kt_quick_panel");
      }

      if (this.extrasUserDisplay && this.extrasUserLayout === "offcanvas") {
        // Init Quick User Panel
        KTLayoutQuickUser.init("kt_quick_user");
      }

      // Init Header Topbar For Mobile Mode
      KTLayoutHeaderTopbar.init("kt_header_mobile_topbar_toggle");
    });
  }
  Withdraw() {
    this.apiService.getData("auth/createPayout").subscribe(
      (result: any) => {
        console.log("myrecord", result);

        const retrievedString = localStorage.getItem("ugcUserobject");
        const parsedObject = JSON.parse(retrievedString);
        parsedObject.balance = 0;
        const strigifiedForStorage = JSON.stringify(parsedObject);
        localStorage.setItem("ugcUserobject", strigifiedForStorage);
        this.apiService.SharingData.next(parsedObject.balance);

        //this.proposals = result;
        //this.loading = true;
        this.cd.detectChanges();
        this.apiService.success("Money withdraw successfully.");
        this.router.navigate(["/dashboard"]);
        // Handle result
      },
      (err: any) => {
        console.log("errormessage", err);
        if (err.error.code == "balance_insufficient") {
          this.apiService.error("Unable to Withdraw,Please Contact Admin");
        } else {
          this.apiService.error(err.error);
        }
      }
    );
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SharedMdropdownComponent } from './shared-mdropdown.component';

describe('SharedMdropdownComponent', () => {
  let component: SharedMdropdownComponent;
  let fixture: ComponentFixture<SharedMdropdownComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SharedMdropdownComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SharedMdropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

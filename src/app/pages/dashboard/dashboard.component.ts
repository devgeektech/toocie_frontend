import { Component, OnInit } from "@angular/core";
import { Title } from "@angular/platform-browser";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.scss"],
})
export class DashboardComponent implements OnInit {
  userObject;
  placeholder;
  constructor(private titleService: Title) {}

  ngOnInit(): void {
    this.placeholder = "Toocie";
    this.userObject = JSON.parse(localStorage.getItem("ugcUserobject"));
    if (this.userObject.userType === "Creater") {
      this.titleService.setTitle(`${this.placeholder} | Creator`);
    } else {
      this.titleService.setTitle(
        `${this.placeholder} | ${this.userObject.userType}`
      );
    }
  }


  
}

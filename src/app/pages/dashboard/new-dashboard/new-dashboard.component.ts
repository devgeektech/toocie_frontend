import { Component, OnInit,ChangeDetectorRef } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';


@Component({
  selector: 'app-new-dashboard',
  templateUrl: './new-dashboard.component.html',
  // styles: ['.font-style{ font-size:20px} .font-style-inner{font-size:15px} ']
  styleUrls: ['./new-dashboard.component.scss'],
})
export class NewDashboardComponent implements OnInit {
  dashboardRes:any;
  userType:string;
  loading=false;
  textInverseCSSClass;
  baseColor = '';
  cssClass;
 
  constructor(private apiService: ApiService,private cd: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.cssClass = `bg-${this.baseColor} ${this.cssClass}`;
    this.textInverseCSSClass = `text-inverse-${this.baseColor}`;
    this.dashboardData();    
    const userDetail = JSON.parse(localStorage.getItem('ugcUserobject'));
    //this.userName = `${userDetail.firstName} ${userDetail.lastName}`;
    this.userType = userDetail.userType;
   }
  dashboardData() {
    this.apiService.getData('auth/DashboardCount').subscribe((res: any) => {
      
      this.dashboardRes = res;

      this.loading = true;
      this.cd.detectChanges();
    },
      error => {
        console.log(error)
      })
  }

}

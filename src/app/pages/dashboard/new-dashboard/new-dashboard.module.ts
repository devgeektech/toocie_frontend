import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewDashboardComponent } from './new-dashboard.component';
import { RouterModule } from '@angular/router';
import { InlineSVGModule } from 'ng-inline-svg';


@NgModule({
  declarations: [NewDashboardComponent],
  imports: [
    CommonModule,
    InlineSVGModule,
    RouterModule.forChild([
      {
        path: '',
        component: NewDashboardComponent,
        data:{
          breadcrumb:'Dashboard',
          title:'Toocie | Login'
        }
        
      },
    ]),
  ],
  
})
export class NewDashboardModule { }

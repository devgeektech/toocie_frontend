import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { DashboardsModule } from '../../_metronic/partials/content/dashboards/dashboards.module';
import { BrowserModule , Title} from '@angular/platform-browser';
import { InlineSVGModule } from 'ng-inline-svg';

@NgModule({
  declarations: [DashboardComponent],
  imports: [
    CommonModule,
    InlineSVGModule,
    RouterModule.forChild([
      {
        path: '',
        component: DashboardComponent,
        data:{
          breadcrumb:'Dashboard',
          title:'Toocie | Login'
        }
        
      },
      // {
      //   path: 'new',
      //   component: NewDashboardComponent,
      //   data:{
      //     breadcrumb:'Dashboard',
      //     title:'Toocie | Login'
      //   }
        
      // },
    ]),
    DashboardsModule,
  ],
  providers: [Title],  

})
export class DashboardModule {}

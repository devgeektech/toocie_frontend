export const FILEEXTENTIONTYPE = ["mov","ogg", "wmv", "mp4", "avi", "mkv", "webm"];

export const FILEERRORMESSAGE = "Invalid File Type.";
export const FILESIZEERRORMESSAGE =
  "File size has exceeded it max limit of 100MB.";

export const FILESIZE = 104857600;

export const DOCEXTENTIONTYPE = ["jpg", "png", "pdf"];
export const DOCERRORMESSAGE = "Invalid File Type.";
export const DOCSIZEERRORMESSAGE =
  "File size has exceeded it max limit of 10MB.";
export const DOCFILESIZE = 10485760;

export const STRIPE_PUBLISHABLE_KEY =
  "pk_live_vwexMoZ49MJIp6NFTxAXxiSS006qAog3xj"
  //"pk_test_AyqtUuJI65Om83qpTNlPdS6K00uVH7ZNMJ";

export const COUNTRYSTATES = [
  [
    { code: "AL", name: "Alabama" },
    { code: "AK", name: "Alaska" },
    { code: "AZ", name: "Arizona" },
    { code: "AR", name: "Arkansas" },
    { code: "AP", name: "Armed Forces (AP)" },
    { code: "CA", name: "California" },
    { code: "CO", name: "Colorado" },
    { code: "CT", name: "Connecticut" },
    { code: "DE", name: "Delaware" },
    { code: "DC", name: "District of Columbia" },
    { code: "FL", name: "Florida" },
    { code: "GA", name: "Georgia" },
    { code: "HI", name: "Hawaii" },
    { code: "ID", name: "Idaho" },
    { code: "IL", name: "Illinois" },
    { code: "IN", name: "Indiana" },
    { code: "IA", name: "Iowa" },
    { code: "KS", name: "Kansas" },
    { code: "KY", name: "Kentucky" },
    { code: "LA", name: "Louisiana" },
    { code: "ME", name: "Maine" },
    { code: "MD", name: "Maryland" },
    { code: "MA", name: "Massachusetts" },
    { code: "MI", name: "Michigan" },
    { code: "MN", name: "Minnesota" },
    { code: "MS", name: "Mississippi" },
    { code: "MO", name: "Missouri" },
    { code: "MT", name: "Montana" },
    { code: "NE", name: "Nebraska" },
    { code: "NV", name: "Nevada" },
    { code: "NH", name: "New Hampshire" },
    { code: "NJ", name: "New Jersey" },
    { code: "NM", name: "New Mexico" },
    { code: "NY", name: "New York" },
    { code: "NC", name: "North Carolina" },
    { code: "ND", name: "North Dakota" },
    { code: "OH", name: "Ohio" },
    { code: "OK", name: "Oklahoma" },
    { code: "OR", name: "Oregon" },
    { code: "PA", name: "Pennsylvania" },
    { code: "PR", name: "Puerto Rico" },
    { code: "RI", name: "Rhode Island" },
    { code: "SC", name: "South Carolina" },
    { code: "SD", name: "South Dakota" },
    { code: "TN", name: "Tennessee" },
    { code: "TX", name: "Texas" },
    { code: "UT", name: "Utah" },
    { code: "VT", name: "Vermont" },
    { code: "VA", name: "Virginia" },
    { code: "WA", name: "Washington" },
    { code: "WV", name: "West Virginia" },
    { code: "WI", name: "Wisconsin" },
    { code: "WY", name: "Wyoming" },
  ],
  [
    { code: "VAN", name: "Antwerp" },
    { code: "BRU", name: "Brussels-Capital Region" },
    { code: "VOV", name: "East Flanders" },
    { code: "VLG", name: "Flanders" },
    { code: "VBR", name: "Flemish Brabant" },
    { code: "WHT", name: "Hainaut" },
    { code: "WLG", name: "Liege" },
    { code: "VLI", name: "Limburg" },
    { code: "WLX", name: "Luxembourg" },
    { code: "WNA", name: "Namur" },
    { code: "WAL", name: "Wallonia" },
    { code: "WBR", name: "Walloon Brabant" },
    { code: "VWV", name: "West Flanders" },
  ],
];


export const BreadCrumTexts = {
  'Transactions': 'Transactions',
  'Payment':'Paiements',
  'Videos':'Vidéos',
  'Campaigns':'Campagnes',
  'Proposals':'Propositions',
  'Communication':'Chat',
  'Reviews':'Avis',
  'Dashboard':'Tableau de bord',
  'DashBoard':'Tableau de bord',
  'Products':'Produits',
  'Review':'Avis',
  'Tags':'Tags',
  'Settings':'Paramètres',
  'Creators':'Createurs',
  'Brands':'Marques'
}

export const NoRecordFoundText = {
  'transactions': 'Transactions',
  'payment':'Paiements',
  'video':'Vidéos',
  'campaign':'Campagnes',
  'proposal':'Propositions',
  'product':'Produits',
  'review':'Avis',
  'tag':'Tags',
  'creator':'Createurs',
  'Brand':'Marques'
}
import { Component, OnInit,ChangeDetectorRef } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
@Component({
  selector: 'app-dashboard2',
  templateUrl: './dashboard2.component.html',
})
export class Dashboard2Component implements OnInit {
  dashboardRes = [];
  userType:string;
  loading=false;
  modData 
  constructor(private apiService: ApiService,private cd: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.dashboardData();
    const userDetail = JSON.parse(localStorage.getItem('ugcUserobject'));
    //this.userName = `${userDetail.firstName} ${userDetail.lastName}`;
    this.userType = userDetail.userType;
   }
  dashboardData() {
    this.apiService.getData('auth/DashboardCount').subscribe((res: any) => {
      
      this.dashboardRes = res;

      this.loading = true;
      this.cd.detectChanges();
    },
      error => {
        console.log(error)
      })
  }
}

import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tiles-widget11',
  templateUrl: './tiles-widget11.component.html',
  styles: ['.font-style{ font-size:20px}  .font-style-inner{font-size:15px}']

})
export class TilesWidget11Component implements OnInit {
  @Input() cssClass = '';
  @Input() data: any;
  @Input() type: any;
  userType:string;

  @Input() widgetHeight = '150px';
  @Input() baseColor = 'success';
  keys
  textInverseCSSClass;

  loading = false
  constructor(
    private router:Router
  ) { }

  ngOnInit() {
    this.cssClass = `bg-${this.baseColor} ${this.cssClass}`;
    this.textInverseCSSClass = `text-inverse-${this.baseColor}`;
    const userDetail = JSON.parse(localStorage.getItem('ugcUserobject'));
    this.userType = userDetail.userType;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['data']) { 
      this.loading = true
    }
  }


  checkClick(){
    alert("dsgdsg");
    debugger;
    this.router.navigate(["/admin/campaigns/list"])
  }

}

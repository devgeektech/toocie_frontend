import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';
import { BreadcrumbItemModel } from '../_models/breadcrumb-item.model';
import { LayoutService } from '../../../../core';
import { SubheaderModel } from '../_models/subheader.model';
import { ActivatedRouteSnapshot, Data, NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
// kt_header_menu
// kt_aside_menu
@Injectable({
  providedIn: 'root',
})
export class SubheaderService implements OnDestroy {
 
   descriptionSubject: BehaviorSubject<string> = new BehaviorSubject<string>('');
  breadCrumbsSubject: BehaviorSubject<
    BreadcrumbItemModel[]
  > = new BehaviorSubject<BreadcrumbItemModel[]>([]);
  subheaderVersionSubject: BehaviorSubject<string> = new BehaviorSubject<
    string
  >('v1'); // [1-6]

  // private fields


  titleNext:''
  titleSubject: BehaviorSubject<string> = new BehaviorSubject<string>(
    'DashBoard'
  );
  private unsubscribe: Subscription[] = []; // Read more: => https://brianflove.com/2016/12/11/anguar-2-unsubscribe-observables/

  constructor(private layout: LayoutService, private router: Router) {
    
    this.router.events.pipe(
      filter((event) => event instanceof NavigationEnd)
    ).subscribe(event => {
      const root = this.router.routerState.snapshot.root;
      const breadcrumbs: BreadcrumbItemModel[] = [];
      this.addBreadcrumb(root, [], breadcrumbs);

      // Emit the new hierarchy
      this.breadCrumbsSubject.next(breadcrumbs);
    });

    this.setDefaultSubheader();
  }




   addBreadcrumb(route: ActivatedRouteSnapshot, parentUrl: string[], breadcrumbs: BreadcrumbItemModel[]) {
    if (route) {
      // Construct the route URL
      const routeUrl = parentUrl.concat(route.url.map(url => url.path));

      // Add an element for the current route part
      if (route.data.breadcrumb) {
        this.titleNext = this.getLabel(route.data)
        this.setTitle( this.titleNext)
        const breadcrumb = {
          title: this.getLabel(route.data),
          linkPath: '/' + routeUrl.join('/'),
          linkText: ''
        };
        breadcrumbs.push(breadcrumb);
      }

      // Add another element for the next route part
      this.addBreadcrumb(route.firstChild, routeUrl, breadcrumbs);
    }
  }

   getLabel(data: Data) {
    // The breadcrumb can be defined as a static string or as a function to construct the breadcrumb element out of the route data
    return typeof data.breadcrumb === 'function' ? data.breadcrumb(data) : data.breadcrumb;
  }




  setDefaultSubheader() {
    this.setSubheaderVersion(this.layout.getProp('subheader.layoutVersion'));
  }

  setBreadcrumbs(breadcrumbs: BreadcrumbItemModel[] = []) {
    this.breadCrumbsSubject.next(breadcrumbs);
  }

  setTitle(title: string = '') {
    if(title == ''){
      this.titleSubject.next(this.titleNext);
    }else{
    this.titleSubject.next(title);
    }
  }




  setDescription(description: string) {
    this.descriptionSubject.next(description);
  }

  private setSubheaderVersion(version: string = 'v1') {
    this.subheaderVersionSubject.next(version);
  }

  // use this method in SubheaderWrapper
  updateAfterRouteChanges(pathName) {
    const aside = this.getBreadcrumbsAndTitle('kt_aside_menu', pathName);
    const header = this.getBreadcrumbsAndTitle('kt_header_menu', pathName);
    // const breadcrumbs =
    //   aside && aside.breadcrumbs.length > 0
    //     ? aside.breadcrumbs
    //     : header.breadcrumbs;

    // this.setBreadcrumbs(breadcrumbs);

    // this.setTitle(
    //   aside && aside.title && aside.title.length > 0
    //     ? aside.title
    //     : header.title
    // );
  }

  private getLinksFromMenu(menu): HTMLAnchorElement[] {
    const parentLiElements = Array.from(
      menu.getElementsByClassName('menu-item-open') || []
    ) as HTMLElement[];
    console.log(Array.from(menu.getElementsByClassName('menu-item-active')))
    const childLiElements = Array.from(
      menu.getElementsByClassName('menu-item-active') || []
    ) as HTMLElement[];




    const result: HTMLAnchorElement[] = [];
    parentLiElements.forEach((el) => {
      const links = Array.from(
        el.getElementsByClassName('menu-link') || []
      ) as HTMLAnchorElement[];
      if (links && links.length > 0) {
        const aLink = links[0];
        if (
          aLink.href &&
          aLink.href.length &&
          aLink.href.length > 0 &&
          aLink.innerHTML !== '/'
        ) {
          result.push(aLink);
        }
      }
    });
    childLiElements.forEach((el) => {
      const links = Array.from(
        el.getElementsByClassName('menu-link') || []
      ) as HTMLAnchorElement[];
      if (links && links.length > 0) {
        const aLink = links[0];
        if (
          aLink.href &&
          aLink.href.length &&
          aLink.href.length > 0 &&
          aLink.innerHTML !== '/'
        ) {
          result.push(aLink);
        }
      }
    });
    return result;
  }

  private getBreadcrumbsAndTitle(menuId, pathName): SubheaderModel {
    const result = new SubheaderModel();
    const menu = document.getElementById(menuId);
    if (!menu) {
      return result;
    }
    const activeLinksArray = this.getLinksFromMenu(menu);




    const activeLinks = activeLinksArray.filter((el) => el.tagName === 'A');
    if (!activeLinks) {
      return result;
    }

    activeLinks.forEach((link) => {
      const titleSpans = link.getElementsByClassName('menu-text');
      if (titleSpans) {
        const titleSpan = Array.from(titleSpans).find(
          (t) => t.innerHTML && t.innerHTML.trim().length > 0
        );
        if (titleSpan) {
          result.breadcrumbs.push({
            title: titleSpan.innerHTML,
            linkPath: link.pathname,
            linkText: titleSpan.innerHTML,
          });
        }
      }
    });
    // result.title = this.getTitle(result.breadcrumbs, pathName);
    // return result;
  }

  private parseUrlAndReturnPathname(href): string {
    const url = document.createElement('a');
    url.href =
      'https://developer.mozilla.org:8080/en-US/search?q=URL#search-results-close-container';

    return url.pathname;
  }

  private getTitle(breadCrumbs, pathname) {
    if (!breadCrumbs || !pathname) {
      return '';
    }

    const length = breadCrumbs.length;
    if (!length) {
      return '';
    }

    return breadCrumbs[length - 1].title;
  }









  ngOnDestroy() {
    this.unsubscribe.forEach((sb) => sb.unsubscribe());
  }
}

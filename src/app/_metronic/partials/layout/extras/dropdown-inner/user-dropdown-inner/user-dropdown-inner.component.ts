import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { LayoutService } from '../../../../../core';
import { UserModel } from '../../../../../../modules/auth/_models/user.model';
import { AuthService } from '../../../../../../modules/auth/_services/auth.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-user-dropdown-inner',
  templateUrl: './user-dropdown-inner.component.html',
  styleUrls: ['./user-dropdown-inner.component.scss'],
})
export class UserDropdownInnerComponent implements OnInit {
  extrasUserDropdownStyle: 'light' | 'dark' = 'light';
  user$: Observable<UserModel>;

  constructor(private layout: LayoutService,private apiService:ApiService, private auth: AuthService, private router: Router, private toastr: ToastrService) {}
  userType:string;
  ngOnInit(): void {
    this.extrasUserDropdownStyle = this.layout.getProp(
      'extras.user.dropdown.style'
    );
    this.user$ = this.auth.currentUserSubject.asObservable();
    const retrievedString = localStorage.getItem("ugcUserobject");
        const parsedObject = JSON.parse(retrievedString);
        this.userType = parsedObject.userType;
  }

  logout() {
    localStorage.removeItem("authorization");
    localStorage.removeItem('ugcUserobject');
    this.router.navigate(["/auth/login"]);
    this.apiService.success('Logged out.');
  }
}
